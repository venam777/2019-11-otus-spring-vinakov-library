package ru.otus.spring.study.vinakov.service;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@Import(BookCommentServiceImpl.class)
@DisplayName("Сервис по работе с комментариями ")
class BookCommentServiceTest {

    @Autowired
    private BookCommentServiceImpl bookCommentService;

    private static final long TEST_BOOK_ID = 1L;

    @Test
    @DisplayName(" умеет добавлять новые комменты")
    void testCreateComments() {
        bookCommentService.addCommentToBook(TEST_BOOK_ID, "Comment 1", 4);
        bookCommentService.addCommentToBook(TEST_BOOK_ID, "Comment 2", 5);
        bookCommentService.addCommentToBook(TEST_BOOK_ID, "Comment 3", 2);
        assertEquals(bookCommentService.findBookComments(TEST_BOOK_ID).size(), 3);
    }

    //остальные методы наверное нет смысла тестировать, т.к. они обертка над DAO. для DAO Тесты есть
}
