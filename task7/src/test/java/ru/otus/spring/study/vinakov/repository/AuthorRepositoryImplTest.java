package ru.otus.spring.study.vinakov.repository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import ru.otus.spring.study.vinakov.domain.Author;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@DisplayName("Repository для работы с авторами должно ")
class AuthorRepositoryImplTest {

    @Autowired
    private AuthorRepository authorRepository;

    private static final String AUTHOR_NAME = "Test author";

    @Test
    void findByName() {
        authorRepository.saveAndFlush(createTestAuthor());
        List<Author> authors = authorRepository.findByName(AUTHOR_NAME);
        assertEquals(authors.size(), 1);
        assertEquals(authors.get(0).getName(), AUTHOR_NAME);
    }

    private Author createTestAuthor() {
        Author author = new Author();
        author.setName(AUTHOR_NAME);
        author.setBirthday(new Date());
        return author;
    }
}