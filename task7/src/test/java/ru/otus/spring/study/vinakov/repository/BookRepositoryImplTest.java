package ru.otus.spring.study.vinakov.repository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import ru.otus.spring.study.vinakov.domain.Author;
import ru.otus.spring.study.vinakov.domain.Book;
import ru.otus.spring.study.vinakov.domain.Genre;
import ru.otus.spring.study.vinakov.util.EntityFactory;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@DisplayName("Repository книг должно ")
class BookRepositoryImplTest {

    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private TestEntityManager entityManager;

    private static Long TEST_BOOK_ID = 1L;
    private static String  TEST_BOOK_NAME = "Test book";

    @Test
    @DisplayName(" искать книги по названию")
    void findByName() {
        Book book = entityManager.find(Book.class, TEST_BOOK_ID);
        entityManager.persist(createTestBook(TEST_BOOK_NAME + "1", "", book.getAuthor(), book.getGenre()));
        entityManager.persist(createTestBook(TEST_BOOK_NAME + "2", "", book.getAuthor(), book.getGenre()));
        entityManager.persist(createTestBook(TEST_BOOK_NAME + "2", "", book.getAuthor(), book.getGenre()));
        assertEquals(bookRepository.findByName(TEST_BOOK_NAME + "1").size(), 1);
        assertEquals(bookRepository.findByName(TEST_BOOK_NAME + "2").size(), 2);
        assertEquals(bookRepository.findByName(TEST_BOOK_NAME + "3").size(), 0);
    }

    private Book createTestBook(String name, String annotation, Author author, Genre genre) {
        Book book = EntityFactory.createEntity(Book.class);
        book.setName(name);
        book.setAnnotation(annotation);
        book.setAuthor(author);
        book.setGenre(genre);
        return book;
    }

}