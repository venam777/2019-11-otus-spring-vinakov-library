package ru.otus.spring.study.vinakov.service;

import org.springframework.stereotype.Component;
import ru.otus.spring.study.vinakov.repository.BookRepository;
import ru.otus.spring.study.vinakov.repository.CommentRepository;
import ru.otus.spring.study.vinakov.domain.Book;
import ru.otus.spring.study.vinakov.domain.Comment;

import javax.persistence.EntityNotFoundException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Component
public class BookCommentServiceImpl implements BookCommentService {

    private final CommentRepository commentRepository;
    private final BookRepository bookRepository;

    public BookCommentServiceImpl(CommentRepository commentRepository, BookRepository bookRepository) {
        this.commentRepository = commentRepository;
        this.bookRepository = bookRepository;
    }

    @Override
    public long addCommentToBook(long bookId, String text, int rating) {
        Optional<Book> book = bookRepository.findById(bookId);
        if (book.isPresent()) {
            Book b = book.get();
            Comment comment = new Comment();
            comment.setText(text);
            comment.setCreated(new Date());
            comment.setRating(rating);
            comment.setBook(b);
            commentRepository.save(comment);
            return comment.getId();
        } else throw new EntityNotFoundException(String.format("Book with id %d not found", bookId));
    }

    @Override
    public List<Comment> findBookComments(long bookId) {
        return commentRepository.findByBookId(bookId);
    }

    @Override
    public double getBookRating(long bookId) {
        return commentRepository.getBookRating(bookId).getValue();
    }

    @Override
    public List<Comment> findCommentsByRating(long bookId, int rating) {
        return commentRepository.findByBookIdAndRating(bookId, rating);
    }

    @Override
    public void deleteCommentsByBook(long bookId) {
        commentRepository.deleteAllByBookId(bookId);
    }
}
