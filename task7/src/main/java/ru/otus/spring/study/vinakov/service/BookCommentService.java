package ru.otus.spring.study.vinakov.service;

import ru.otus.spring.study.vinakov.domain.Comment;

import java.util.List;

public interface BookCommentService {

    long addCommentToBook(long bookId, String text, int rating);

    List<Comment> findBookComments(long bookId);

    double getBookRating(long bookId);

    List<Comment> findCommentsByRating(long bookId, int rating);

    void deleteCommentsByBook(long bookId);

}
