package ru.otus.spring.study.vinakov.shell;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import ru.otus.spring.study.vinakov.repository.GenreRepository;
import ru.otus.spring.study.vinakov.domain.Genre;
import ru.otus.spring.study.vinakov.util.EntityPrinter;
import ru.otus.spring.study.vinakov.util.Messages;

import java.util.stream.Collectors;

@ShellComponent
public class GenreShellOperations {

    private final GenreRepository genreRepository;

    public GenreShellOperations(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }

    @ShellMethod(value = "Добавление нового жанра", key = {"create-genre"})
    public String createGenre(String name) {
        try {
            Genre genre = new Genre();
            genre.setName(name);
            genreRepository.save(genre);
            return Messages.entityCreated("Жанр", genre.getId());
        } catch (Exception e) {
            return Messages.unexpectedError(e.getMessage());
        }
    }

    @ShellMethod(value = "Просмотр всех жанров", key = "list-genre")
    public String findAllGenres() {
        return genreRepository.findAll().stream().map(EntityPrinter::printFull).collect(Collectors.joining("\n"));
    }

}
