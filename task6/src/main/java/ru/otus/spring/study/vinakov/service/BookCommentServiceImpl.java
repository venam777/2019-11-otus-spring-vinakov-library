package ru.otus.spring.study.vinakov.service;

import org.springframework.stereotype.Component;
import ru.otus.spring.study.vinakov.dao.BookDao;
import ru.otus.spring.study.vinakov.dao.CommentDao;
import ru.otus.spring.study.vinakov.domain.Book;
import ru.otus.spring.study.vinakov.domain.Comment;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Component
public class BookCommentServiceImpl implements BookCommentService {

    private final CommentDao commentDao;
    private final BookDao bookDao;
    private EntityManager entityManager;

    public BookCommentServiceImpl(CommentDao commentDao, BookDao bookDao, EntityManager entityManager) {
        this.commentDao = commentDao;
        this.bookDao = bookDao;
        this.entityManager = entityManager;
    }

    @Override
    public long addCommentToBook(long bookId, String text, int rating) {
        Optional<Book> book = Optional.ofNullable(bookDao.find(bookId));
        if (book.isPresent()) {
            Book b = book.get();
            Comment comment = new Comment();
            comment.setText(text);
            comment.setCreated(new Date());
            comment.setRating(rating);
            comment.setBook(b);
            commentDao.create(comment);
            return comment.getId();
        } else throw new EntityNotFoundException(String.format("Book with id %d not found", bookId));
    }

    @Override
    public List<Comment> findBookComments(long bookId) {
        return commentDao.findByBook(bookId);
    }

    @Override
    public double getBookRating(long bookId) {
        return commentDao.getBookRating(bookId);
    }

    @Override
    public List<Comment> findCommentsByRating(long bookId, int rating) {
        return commentDao.findByBookAndRating(bookId, rating);
    }

    @Override
    public void deleteCommentsByBook(long bookId) {
        commentDao.deleteAllByBook(bookId);
    }
}
