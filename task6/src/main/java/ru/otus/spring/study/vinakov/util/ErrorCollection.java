package ru.otus.spring.study.vinakov.util;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

public class ErrorCollection {

    private Set<String> errors;

    public ErrorCollection() {
        errors = new LinkedHashSet<>();
    }

    public ErrorCollection addError(String message) {
        errors.add(message);
        return this;
    }

    public Set<String> getErrors() {
        return Collections.unmodifiableSet(errors);
    }

    public boolean hasErrors() {
        return errors.size() > 0;
    }

}
