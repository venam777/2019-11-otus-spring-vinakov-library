package ru.otus.spring.study.vinakov.dao;

import org.springframework.stereotype.Repository;
import ru.otus.spring.study.vinakov.domain.Author;
import ru.otus.spring.study.vinakov.util.Constants;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public class AuthorDaoImpl implements AuthorDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public long create(Author author) {
        if (author.getId() == Constants.ENTITY_FAKE_ID) {
            entityManager.persist(author);
        } else {
            author = entityManager.merge(author);
        }
        return author.getId();
    }

    @Override
    public Author find(long id) {
        return entityManager.find(Author.class, id);
    }

    @Override
    public void update(Author author) {
        entityManager.merge(author);
    }

    @Override
    public void delete(long id) {
        Query query = entityManager.createQuery("delete from Author a where a.id = :id");
        query.setParameter("id", id);
        query.executeUpdate();
    }

    @Override
    public Collection<Author> findAll() {
        return entityManager.createQuery("select a from Author a", Author.class).getResultList();
    }

    @Override
    public List<Author> findByName(String name) {
        TypedQuery<Author> query = entityManager.createQuery("select a from Author a where a.name = :name", Author.class);
        query.setParameter("name", name);
        return query.getResultList();
    }

    @Override
    public long count() {
        return Optional.ofNullable((Long) entityManager.createQuery("select count(a) from Author a").getSingleResult()).orElse(0L);
    }
}
