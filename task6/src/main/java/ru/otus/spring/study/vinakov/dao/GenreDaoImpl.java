package ru.otus.spring.study.vinakov.dao;

import org.springframework.stereotype.Repository;
import ru.otus.spring.study.vinakov.domain.Genre;
import ru.otus.spring.study.vinakov.util.Constants;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public class GenreDaoImpl implements GenreDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public long create(Genre genre) {
        if (genre.getId() == Constants.ENTITY_FAKE_ID) {
            entityManager.persist(genre);
        } else {
            genre = entityManager.merge(genre);
        }
        return genre.getId();
    }

    @Override
    public Genre find(long id) {
        return entityManager.find(Genre.class, id);
    }

    @Override
    public void update(Genre genre) {
        entityManager.merge(genre);
    }

    @Override
    public void delete(long id) {
        entityManager.createQuery("delete from Genre g where g.id = :id").setParameter("id", id).executeUpdate();
    }

    @Override
    public Collection<Genre> findAll() {
        TypedQuery<Genre> query = entityManager.createQuery("select g from Genre g", Genre.class);
        return query.getResultList();
    }

    @Override
    public long count() {
        Query query = entityManager.createQuery("select count(g) from Genre g");
        return Optional.ofNullable((Long) query.getSingleResult()).orElse(0L);
    }

    @Override
    public List<Genre> findByName(String name) {
        return entityManager.createQuery("select g from Genre g where g.name = :name", Genre.class).setParameter("name", name).getResultList();
    }
}
