package ru.otus.spring.study.vinakov.dao;

import ru.otus.spring.study.vinakov.domain.Book;
import ru.otus.spring.study.vinakov.domain.Comment;
import ru.otus.spring.study.vinakov.dto.BookRating;

import java.util.List;

public interface CommentDao {

    long create(Comment comment);

    Comment find(long id);

    void update(Comment comment);

    void delete(long id);

    List<Comment> findByBook(long bookId);

    double getBookRating(long bookId);

    List<Comment> findByBookAndRating(long bookId, int rating);

    List<Comment> findLastCommentsByBook(long bookId, int count);

    List<BookRating> findTopRatingBooks(int count);

    void deleteAllByBook(long bookId);

    long count();
}
