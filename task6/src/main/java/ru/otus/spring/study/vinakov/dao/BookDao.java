package ru.otus.spring.study.vinakov.dao;

import ru.otus.spring.study.vinakov.domain.Book;

import java.util.Collection;
import java.util.List;

public interface BookDao {

    long create(Book Book);

    Book find(long id);

//    Book findWithComments(long id);

    void update(Book Book);

    void delete(long id);

    Collection<Book> findAll();

    List<Book> findByName(String name);

    long count();

}
