package ru.otus.spring.study.vinakov.dao;

import org.springframework.stereotype.Repository;
import ru.otus.spring.study.vinakov.domain.Book;
import ru.otus.spring.study.vinakov.util.Constants;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public class BookDaoImpl implements BookDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public long create(Book book) {
        if (book.getId() == Constants.ENTITY_FAKE_ID) {
            entityManager.persist(book);
        } else {
            book = entityManager.merge(book);
        }
        return book.getId();
    }

    @Override
    public Book find(long id) {
        return entityManager.find(Book.class, id);
    }

    @Override
    public void update(Book book) {
        entityManager.merge(book);
    }

    @Override
    public void delete(long id) {
        entityManager.createQuery("delete from Book b where b.id = :id").setParameter("id", id).executeUpdate();
    }

    @Override
    public Collection<Book> findAll() {
        //чисто для того, чтобы поупражняться с EntityGraph. А так я сделал бы жанр с EAGER подгрузкой
        TypedQuery<Book> query = entityManager.createQuery("select b from Book b join fetch b.author", Book.class);
        EntityGraph entityGraph = entityManager.getEntityGraph("Book.genre");
        query.setHint("javax.persistence.fetchgraph", entityGraph);
        return query.getResultList();
    }

    @Override
    public List<Book> findByName(String name) {
        return entityManager.createQuery("select b from Book b where b.name = :name", Book.class).setParameter("name", name).getResultList();
    }

    @Override
    public long count() {
        return Optional.ofNullable((Long) entityManager.createQuery("select count(b) from Book b").getSingleResult()).orElse(0L);
    }
}
