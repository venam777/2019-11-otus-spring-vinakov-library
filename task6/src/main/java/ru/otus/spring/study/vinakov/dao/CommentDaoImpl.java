package ru.otus.spring.study.vinakov.dao;

import org.springframework.stereotype.Repository;
import ru.otus.spring.study.vinakov.domain.Book;
import ru.otus.spring.study.vinakov.domain.Comment;
import ru.otus.spring.study.vinakov.dto.BookRating;
import ru.otus.spring.study.vinakov.util.Constants;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public class CommentDaoImpl implements CommentDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public long create(Comment comment) {
        if (comment.getId() == Constants.ENTITY_FAKE_ID) {
            entityManager.persist(comment);
        } else {
            comment = entityManager.merge(comment);
        }
        return comment.getId();
    }

    @Override
    public Comment find(long id) {
        return entityManager.createQuery("select c from Comment c where c.id = :id", Comment.class).setParameter("id", id).getSingleResult();
    }

    @Override
    public void update(Comment comment) {
        entityManager.merge(comment);
    }

    @Override
    public void delete(long id) {
        entityManager.createQuery("delete from Comment c where c.id = :id").setParameter("id", id).executeUpdate();
    }

    @Override
    public List<Comment> findByBook(long bookId) {
        return entityManager.createQuery("select c from Comment c where c.book.id = :id", Comment.class).setParameter("id", bookId).getResultList();
    }

    @Override
    public double getBookRating(long bookId) {
        return Optional.ofNullable((Double) entityManager.createQuery("select avg(c.rating) from Comment c where c.book.id = :bookId").setParameter("bookId", bookId).getSingleResult()).orElse(0d);
    }

    @Override
    public List<Comment> findByBookAndRating(long bookId, int rating) {
        return entityManager.createQuery("select c from Comment c where c.book.id = :bookId and c.rating = :rating", Comment.class)
                .setParameter("bookId", bookId)
                .setParameter("rating", rating)
                .getResultList();
    }

    @Override
    public List<Comment> findLastCommentsByBook(long bookId, int count) {
        return entityManager.createQuery("select c from Comment c where c.book.id = :bookId", Comment.class)
                .setParameter("bookId", bookId)
                .setMaxResults(count)
                .getResultList();
    }

    @Override
    public List<BookRating> findTopRatingBooks(int count) {
        return entityManager.createQuery("select new ru.otus.spring.study.vinakov.dto.BookRating( c.book, avg(c.rating))" +
                " from Comment c group by c.book order by avg(c.rating) desc", BookRating.class).setMaxResults(count).getResultList();
    }

    @Override
    public void deleteAllByBook(long bookId) {
        entityManager.createQuery("delete from Comment c where c.book.id = :bookId").setParameter("bookId", bookId).executeUpdate();
    }

    @Override
    public long count() {
        return Optional.ofNullable((Long) entityManager.createQuery("select count(c) from Comment c").getSingleResult()).orElse(0L);
    }
}
