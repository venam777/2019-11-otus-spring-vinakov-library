package ru.otus.spring.study.vinakov.shell;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import ru.otus.spring.study.vinakov.service.BookCommentService;
import ru.otus.spring.study.vinakov.util.EntityPrinter;
import ru.otus.spring.study.vinakov.util.Messages;

import javax.persistence.EntityNotFoundException;

@ShellComponent
public class CommentShellOperations {

    private BookCommentService bookCommentService;

    private static final String COMMENT_ENTITY = "Комментарий";
    private static final String BOOK_ENTITY = "Книга";

    public CommentShellOperations(BookCommentService bookCommentService) {
        this.bookCommentService = bookCommentService;
    }

    @ShellMethod(value = "Добавление нового комментария к книге", key = {"add-comment"})
    public String addCommentToBook(long bookId, String text, int rating) {
        try {
            long id = bookCommentService.addCommentToBook(bookId, text, rating);
            return Messages.entityCreated(COMMENT_ENTITY, id);
        } catch (EntityNotFoundException e) {
            return Messages.entityNotFoundError(BOOK_ENTITY);
        } catch (Exception e) {
            return Messages.unexpectedError(e.getMessage());
        }
    }

    @ShellMethod(value = "Поиск комментариев по книге", key = {"list-comment"})
    public String findCommentsByBook(long bookId) {
        try {
            StringBuilder builder = new StringBuilder();
            bookCommentService.findBookComments(bookId).forEach(c -> builder.append(EntityPrinter.printFull(c)).append("\n"));
            return builder.toString();
        } catch (EntityNotFoundException e) {
            return Messages.entityNotFoundError(BOOK_ENTITY);
        } catch (Exception e) {
            return Messages.unexpectedError(e.getMessage());
        }
    }

    @ShellMethod(value = "Рейтинг книги на основе оценок из комментариев", key = {"book-rating"})
    public String getBookRating(long bookId) {
        try {
            double rating = bookCommentService.getBookRating(bookId);
            return rating == 0 ? "У данной книги пока нет оценок" : "Рейтинг книги: " + rating;
        } catch (EntityNotFoundException e) {
            return Messages.entityNotFoundError(BOOK_ENTITY);
        } catch (Exception e) {
            return Messages.unexpectedError(e.getMessage());
        }
    }

    @ShellMethod(value = "Удаление всех комментариев к книге", key = {"delete-book-comments", "dbc"})
    public String deleteAllCommentsByBook(long bookId) {
        try {
            bookCommentService.deleteCommentsByBook(bookId);
            return "Комментарии успешно удалены";
        } catch (EntityNotFoundException e) {
            return Messages.entityNotFoundError(BOOK_ENTITY);
        } catch (Exception e) {
            return Messages.unexpectedError(e.getMessage());
        }
    }
}
