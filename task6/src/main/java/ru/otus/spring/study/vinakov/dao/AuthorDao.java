package ru.otus.spring.study.vinakov.dao;

import ru.otus.spring.study.vinakov.domain.Author;

import java.util.Collection;
import java.util.List;

public interface AuthorDao {

    long create(Author author);

    Author find(long id);

    void update(Author author);

    void delete(long id);

    Collection<Author> findAll();

    List<Author> findByName(String name);

    long count();
}
