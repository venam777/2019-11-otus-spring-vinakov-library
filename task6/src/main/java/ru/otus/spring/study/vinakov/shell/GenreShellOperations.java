package ru.otus.spring.study.vinakov.shell;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import ru.otus.spring.study.vinakov.dao.GenreDao;
import ru.otus.spring.study.vinakov.domain.Genre;
import ru.otus.spring.study.vinakov.util.EntityPrinter;
import ru.otus.spring.study.vinakov.util.Messages;

import java.util.stream.Collectors;

@ShellComponent
public class GenreShellOperations {

    private GenreDao genreDao;

    public GenreShellOperations(GenreDao genreDao) {
        this.genreDao = genreDao;
    }

    @ShellMethod(value = "Добавление нового жанра", key = {"create-genre"})
    public String createGenre(String name) {
        try {
            Genre genre = new Genre();
            genre.setName(name);
            Long id = genreDao.create(genre);
            return Messages.entityCreated("Жанр", id);
        } catch (Exception e) {
            return Messages.unexpectedError(e.getMessage());
        }
    }

    @ShellMethod(value = "Просмотр всех жанров", key = "list-genre")
    public String findAllGenres() {
        return genreDao.findAll().stream().map(EntityPrinter::printFull).collect(Collectors.joining("\n"));
    }

}
