package ru.otus.spring.study.vinakov.dao;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import ru.otus.spring.study.vinakov.domain.Author;
import ru.otus.spring.study.vinakov.domain.Book;
import ru.otus.spring.study.vinakov.domain.Genre;
import ru.otus.spring.study.vinakov.util.EntityFactory;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Import(BookDaoImpl.class)
@DisplayName("DAO книг должно ")
class BookDaoImplTest {

    @Autowired
    private BookDaoImpl dao;
    @Autowired
    private TestEntityManager entityManager;

    private static Long TEST_BOOK_ID = 1L;
    private static String  TEST_BOOK_NAME = "Test book";
    private static Long TOTAL_BOOK_COUNT = 2L;

    @Test
    @DisplayName(" корректно создавать книгу")
    void testCreate() {
        Book book = entityManager.find(Book.class, TEST_BOOK_ID);
        Book newBook = createTestBook(TEST_BOOK_NAME, book.getAnnotation(), book.getAuthor(), book.getGenre());
        long id = dao.create(newBook);
        Book createdBook = entityManager.find(Book.class, id);
        assertEquals(createdBook.getName(), TEST_BOOK_NAME);
        assertEquals(createdBook.getAnnotation(), book.getAnnotation());
        assertEquals(createdBook.getAuthor().getId(), book.getAuthor().getId());
        assertEquals(createdBook.getGenre().getId(), book.getGenre().getId());
    }

    @Test
    @DisplayName(" находить книги вместе с авторами и жанрами")
    void testFind() {
        Book book = dao.find(TEST_BOOK_ID);
        assertTrue(book != null && book.getGenre() != null && book.getAuthor() != null && book.getAuthor().getName() != null && book.getGenre().getName() != null);
    }

    @Test
    @DisplayName(" обновлять книги")
    void testUpdate() {
        Book book = entityManager.find(Book.class, TEST_BOOK_ID);
        Book newBook = createTestBook(TEST_BOOK_NAME, book.getAnnotation(), book.getAuthor(), book.getGenre());
        entityManager.persist(newBook);
        newBook.setName("SUPER BOOK");
        dao.update(newBook);
        assertEquals(entityManager.find(Book.class, newBook.getId()).getName(), "SUPER BOOK");
    }

    @Test
    @DisplayName(" находить все книги в БД")
    void findAll() {
        assertEquals(dao.findAll().size(), 2);
    }

    @Test
    @DisplayName(" удалять книги")
    void testDelete() {
        dao.delete(TEST_BOOK_ID);
        assertNull(entityManager.find(Book.class, TEST_BOOK_ID));
    }

    @Test
    @DisplayName(" считать количество книг в БД")
    void count() {
        assertEquals(dao.count(), TOTAL_BOOK_COUNT);
        Book book = entityManager.find(Book.class, TEST_BOOK_ID);
        entityManager.persist(createTestBook(TEST_BOOK_NAME, book.getAnnotation(), book.getAuthor(), book.getGenre()));
        assertEquals(dao.count(), TOTAL_BOOK_COUNT + 1);
    }


    @Test
    @DisplayName(" искать книги по названию")
    void findByName() {
        Book book = entityManager.find(Book.class, TEST_BOOK_ID);
        entityManager.persist(createTestBook(TEST_BOOK_NAME + "1", "", book.getAuthor(), book.getGenre()));
        entityManager.persist(createTestBook(TEST_BOOK_NAME + "2", "", book.getAuthor(), book.getGenre()));
        entityManager.persist(createTestBook(TEST_BOOK_NAME + "2", "", book.getAuthor(), book.getGenre()));
        assertEquals(dao.findByName(TEST_BOOK_NAME + "1").size(), 1);
        assertEquals(dao.findByName(TEST_BOOK_NAME + "2").size(), 2);
        assertEquals(dao.findByName(TEST_BOOK_NAME + "3").size(), 0);
    }

    private Book createTestBook(String name, String annotation, Author author, Genre genre) {
        Book book = EntityFactory.createEntity(Book.class);
        book.setName(name);
        book.setAnnotation(annotation);
        book.setAuthor(author);
        book.setGenre(genre);
        return book;
    }

}