package ru.otus.spring.study.vinakov.dao;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import ru.otus.spring.study.vinakov.domain.Author;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@DisplayName("Дао для работы с авторами должно ")
@Import(AuthorDaoImpl.class)
class AuthorDaoImplTest {

    @Autowired
    private AuthorDao dao;
    @Autowired
    private TestEntityManager entityManager;

    private static final Long COUNT = 3L;
    private static final String AUTHOR_NAME = "Test author";

    @Test
    @DisplayName(" сохранять в БД объект и возвращать его ID")
    void testCreate() {
        Author author = createTestAuthor();
        long id = dao.create(author);
        Author created = entityManager.find(Author.class, id);
        assertThat(author).isEqualToComparingFieldByField(created);
    }

    @Test
    @DisplayName(" корректно находить объект в БД по его ID")
    void testFind() {
        Author author = createTestAuthor();
        entityManager.persist(author);
        Author createdAuthor = dao.find(author.getId());
        assertThat(author).isEqualToComparingFieldByField(createdAuthor);
    }

    @Test
    void testUpdate() {
        Author author = createTestAuthor();
        entityManager.persist(author);
        author.setName("Updated author");
        dao.update(author);
        assertEquals(author.getName(), "Updated author");
    }

    @Test
    void testDelete() {
        Author author = createTestAuthor();
        entityManager.persist(author);
        assertEquals(dao.count(), COUNT + 1);
        dao.delete(author.getId());
        assertEquals(dao.count(), COUNT);
    }

    @Test
    void findAll() {
        Collection<Author> authors = dao.findAll();
        assertNotNull(authors);
        assertEquals(authors.size(), COUNT);
        authors.forEach(a -> assertTrue(a.getName() != null && a.getBirthday() != null));
    }

    @Test
    void findByName() {
        dao.create(createTestAuthor());
        List<Author> authors = dao.findByName(AUTHOR_NAME);
        assertEquals(authors.size(), 1);
        assertEquals(authors.get(0).getName(), AUTHOR_NAME);
    }

    private Author createTestAuthor() {
        Author author = new Author();
        author.setName(AUTHOR_NAME);
        author.setBirthday(new Date());
        return author;
    }
}