package ru.otus.spring.study.vinakov.dao;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.Import;
import ru.otus.spring.study.vinakov.domain.Author;
import ru.otus.spring.study.vinakov.util.Constants;
import ru.otus.spring.study.vinakov.util.EntityFactory;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@JdbcTest
@DisplayName("Дао для работы с авторами должно ")
@Import(AuthorDaoImpl.class)
class AuthorDaoImplTest {

    @Autowired
    private AuthorDao dao;

    private static final Long COUNT = 3L;
    private static final String AUTHOR_NAME = "Test author";

    @Test
    @DisplayName(" создавать пустой объект-заглушку")
    void testCreateEmpty() {
        Author author = EntityFactory.createEntity(Author.class);
        assertEquals(author.getId(), (long) Constants.ENTITY_FAKE_ID);
    }

    @Test
    @DisplayName(" сохранять в БД объект и возвращать его ID")
    void testCreate() {
        Author author = createTestAuthor();
        long id = dao.create(author);
        assertNotEquals(id, author.getId());
    }

    @Test
    @DisplayName(" корректно находить объект в БД по его ID")
    void testFind() {
        Author author = createTestAuthor();
        long id = dao.create(author);
        Author createdAuthor = dao.find(id);
        assertEquals(author.getName(), createdAuthor.getName());
        assertNotEquals(author.getId(), createdAuthor.getId());
    }

    @Test
    void update() {
        Author author = createTestAuthor();
        long id = dao.create(author);
        Author newAuthor = new Author(id, author.getName(), author.getBirthday());
        newAuthor.setName("Updated author");
        dao.update(newAuthor);
        Author updatedAuthor = dao.find(id);
        assertEquals(updatedAuthor.getName(), newAuthor.getName());
    }

    @Test
    void testDelete() {
        Author author = createTestAuthor();
        long id = dao.create(author);
        assertEquals(dao.count(), COUNT + 1);
        dao.delete(id);
        assertEquals(dao.count(), COUNT);
    }

    @Test
    void findAll() {
        Collection<Author> authors = dao.findAll();
        assertNotNull(authors);
        assertEquals(authors.size(), COUNT);
        authors.forEach(a -> assertTrue(a.getName() != null && a.getBirthday() != null));
    }

    @Test
    void findByName() {
        dao.create(createTestAuthor());
        List<Author> authors = dao.findByName(AUTHOR_NAME);
        assertEquals(authors.size(), 1);
        assertEquals(authors.get(0).getName(), AUTHOR_NAME);
    }

    private Author createTestAuthor() {
        Author author = EntityFactory.createEntity(Author.class);
        author.setName(AUTHOR_NAME);
        author.setBirthday(new Date());
        return author;
    }
}