package ru.otus.spring.study.vinakov.dao;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.context.annotation.Import;
import org.springframework.dao.EmptyResultDataAccessException;
import ru.otus.spring.study.vinakov.domain.Author;
import ru.otus.spring.study.vinakov.domain.Book;
import ru.otus.spring.study.vinakov.domain.Genre;
import ru.otus.spring.study.vinakov.util.EntityFactory;

import static org.junit.jupiter.api.Assertions.*;

@JdbcTest
@Import(BookDaoImpl.class)
@DisplayName("DAO книг должно ")
class BookDaoImplTest {

    @Autowired
    private BookDaoImpl dao;

    private static Long TEST_BOOK_ID = 1L;
    private static String  TEST_BOOK_NAME = "Test book";
    private static Long TEST_AUTHOR_ID = 1L;
    private static Long TEST_GENRE_ID = 1L;
    private static Long TOTAL_BOOK_COUNT = 2L;

    @Test
    @DisplayName(" корректно создавать книгу")
    void testCreate() {
        Book book = createTestBook(TEST_BOOK_NAME);
        long id = dao.create(book);
        Book createdBook = dao.find(id);
        assertEquals(createdBook.getName(), book.getName());
        assertEquals(createdBook.getAnnotation(), book.getAnnotation());
        assertEquals(createdBook.getAuthor().getId(), book.getAuthor().getId());
        assertEquals(createdBook.getGenre().getId(), book.getGenre().getId());
    }

    @Test
    @DisplayName(" находить книги вместе с авторами и жанрами")
    void testFind() {
        Book book = dao.find(TEST_BOOK_ID);
        assertTrue(book != null && book.getGenre() != null && book.getAuthor() != null && book.getAuthor().getName() != null && book.getGenre().getName() != null);
    }

    @Test
    @DisplayName(" обновлять книги")
    void testUpdate() {
        Book book = createTestBook(TEST_BOOK_NAME);
        long id = dao.create(book);
        Book createdBook = dao.find(id);
        createdBook.setName("SUPER BOOK");
        dao.update(createdBook);
        assertEquals(dao.find(id).getName(), "SUPER BOOK");
    }

    @Test
    @DisplayName(" находить все книги в БД")
    void findAll() {
        assertEquals(dao.findAll().size(), 2);
    }

    @Test
    @DisplayName(" удалять книги")
    void testDelete() {
        dao.delete(TEST_BOOK_ID);
        assertThrows(EmptyResultDataAccessException.class, () -> dao.find(TEST_BOOK_ID));
    }

    @Test
    @DisplayName(" считать количество книг в БД")
    void count() {
        assertEquals(dao.count(), TOTAL_BOOK_COUNT);
        dao.create(createTestBook(TEST_BOOK_NAME));
        assertEquals(dao.count(), TOTAL_BOOK_COUNT + 1);
    }


    @Test
    @DisplayName(" искать книги по названию")
    void findByName() {
        dao.create(createTestBook(TEST_BOOK_NAME + "1"));
        dao.create(createTestBook(TEST_BOOK_NAME + "2"));
        dao.create(createTestBook(TEST_BOOK_NAME + "2"));
        assertEquals(dao.findByName(TEST_BOOK_NAME + "1").size(), 1);
        assertEquals(dao.findByName(TEST_BOOK_NAME + "2").size(), 2);
        assertEquals(dao.findByName(TEST_BOOK_NAME + "3").size(), 0);
    }


    private Book createTestBook(String name) {
        Book book = EntityFactory.createEntity(Book.class);
        book.setName(name);
        book.setAnnotation("annotation");
        book.setAuthor(new Author(TEST_AUTHOR_ID));
        book.setGenre(new Genre(TEST_GENRE_ID));
        return book;
    }
}