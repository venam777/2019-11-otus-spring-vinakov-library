INSERT INTO AUTHOR(NAME, BIRTHDAY) VALUES ('Пушкин Александр Сергеевич', PARSEDATETIME('06-06-1799','dd-MM-yyyy'));
INSERT INTO AUTHOR(NAME, BIRTHDAY) VALUES ('Лермонтов Михаил Юрьевич', PARSEDATETIME('15-10-1814','dd-MM-yyyy'));
INSERT INTO AUTHOR(NAME, BIRTHDAY) VALUES ('Джон Рональд Руэл Толкин', PARSEDATETIME('03-01-1892','dd-MM-yyyy'));

INSERT INTO GENRE(NAME) VALUES ('Фантастика');
INSERT INTO GENRE(NAME) VALUES ('Роман');
INSERT INTO GENRE(NAME) VALUES ('Фентези');
INSERT INTO GENRE(NAME) VALUES ('Исторический роман');
INSERT INTO GENRE(NAME) VALUES ('Документальный');
INSERT INTO GENRE(NAME) VALUES ('Поэзия');
INSERT INTO GENRE(NAME) VALUES ('Проза');

INSERT INTO BOOK(NAME, ANNOTATION, GENRE_ID, AUTHOR_ID) VALUES ('Евгений Онегин', '«Евгений Онегин» — роман в стихах русского поэта Александра Сергеевича Пушкина, написанный в 1823—1830 годах, одно из самых значительных произведений русской словесности.',
                                                                (SELECT ID FROM GENRE WHERE NAME = 'Роман'), (SELECT ID FROM AUTHOR WHERE NAME = 'Пушкин Александр Сергеевич'));

INSERT INTO BOOK(NAME, ANNOTATION, GENRE_ID, AUTHOR_ID) VALUES ('Мцыри', '«Мцыри» — романтическая поэма М. Ю. Лермонтова, написанная в 1839 году',
                                                                (SELECT ID FROM GENRE WHERE NAME = 'Поэзия'), (SELECT ID FROM AUTHOR WHERE NAME = 'Лермонтов Михаил Юрьевич'));

INSERT INTO READER(NAME, BIRTHDAY, REGISTERED) VALUES ('Иванов Иван Иванович', PARSEDATETIME('15-10-1994','dd-MM-yyyy'), PARSEDATETIME('12-05-2014','dd-MM-yyyy'));
INSERT INTO READER(NAME, BIRTHDAY, REGISTERED) VALUES ('Федоров Василий Сергеевич', PARSEDATETIME('07-10-1990','dd-MM-yyyy'), PARSEDATETIME('14-07-2016','dd-MM-yyyy'));
INSERT INTO READER(NAME, BIRTHDAY, REGISTERED) VALUES ('Петров Сергей Михайлович', PARSEDATETIME('01-02-1995','dd-MM-yyyy'), PARSEDATETIME('16-02-2018','dd-MM-yyyy'));