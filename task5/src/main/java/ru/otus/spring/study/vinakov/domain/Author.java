package ru.otus.spring.study.vinakov.domain;

import java.util.Date;

public class Author {

    private long id;
    private String name;
    private Date birthday;

    public Author(long id) {
        this.id = id;
    }

    public Author(long id, String name, Date birthday) {
        this.id = id;
        this.name = name;
        this.birthday = birthday;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

}
