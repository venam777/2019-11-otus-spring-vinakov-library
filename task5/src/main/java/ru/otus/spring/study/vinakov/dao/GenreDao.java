package ru.otus.spring.study.vinakov.dao;

import ru.otus.spring.study.vinakov.domain.Genre;

import java.util.Collection;
import java.util.List;

public interface GenreDao {

    long create(Genre Genre);

    Genre find(long id);

    void update(Genre Genre);

    void delete(long id);

    Collection<Genre> findAll();

    List<Genre> findByName(String name);

    long count();

}
