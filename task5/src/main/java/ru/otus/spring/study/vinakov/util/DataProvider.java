package ru.otus.spring.study.vinakov.util;

public interface DataProvider<T> {

    T provide();
}
