package ru.otus.spring.study.vinakov.shell;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import ru.otus.spring.study.vinakov.dao.AuthorDao;
import ru.otus.spring.study.vinakov.domain.Author;
import ru.otus.spring.study.vinakov.util.Constants;
import ru.otus.spring.study.vinakov.util.EntityPrinter;
import ru.otus.spring.study.vinakov.util.Messages;

import java.text.ParseException;
import java.util.stream.Collectors;

import static ru.otus.spring.study.vinakov.util.Constants.DATE_FORMAT;

@ShellComponent
public class AuthorShellOperations {

    private AuthorDao authorDao;

    public AuthorShellOperations(AuthorDao authorDao) {
        this.authorDao = authorDao;
    }

    @ShellMethod(value = "Добавление нового автора", key = {"create-author"})
    public String createAuthor(String name, @ShellOption(help = "Формат даты dd.mm.yyyy") String birthday) {
        try {
            long id = authorDao.create(new Author(Constants.ENTITY_FAKE_ID, name, DATE_FORMAT.parse(birthday)));
            return Messages.entityCreated("Автор", id);
        } catch (ParseException e) {
            return "Ошибка чтения даты! Дата указывается в формате " + Constants.DATE_FORMAT_PATTER;
        } catch (Exception e) {
            return Messages.unexpectedError(e.getMessage());
        }
    }

    @ShellMethod(value = "Просмотр всех авторов", key = "list-author")
    public String findAllAuthors() {
        return authorDao.findAll().stream().map(EntityPrinter::printFull).collect(Collectors.joining("\n"));
    }

}
