package ru.otus.spring.study.vinakov.shell;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import ru.otus.spring.study.vinakov.dao.AuthorDao;
import ru.otus.spring.study.vinakov.dao.BookDao;
import ru.otus.spring.study.vinakov.dao.GenreDao;
import ru.otus.spring.study.vinakov.domain.Author;
import ru.otus.spring.study.vinakov.domain.Book;
import ru.otus.spring.study.vinakov.domain.Genre;
import ru.otus.spring.study.vinakov.exception.EntityNotFoundException;
import ru.otus.spring.study.vinakov.util.EntityFactory;
import ru.otus.spring.study.vinakov.util.EntityPrinter;
import ru.otus.spring.study.vinakov.util.Messages;

import java.util.Optional;
import java.util.stream.Collectors;

@ShellComponent
public class BookShellOperations {

    private BookDao bookDao;
    private AuthorDao authorDao;
    private GenreDao genreDao;

    private static final String AUTHOR_ENTITY = "Автор";
    private static final String GENRE_ENTITY = "Жанр";
    private static final String BOOK_ENTITY = "Книга";

    public BookShellOperations(BookDao bookDao, AuthorDao authorDao, GenreDao genreDao) {
        this.bookDao = bookDao;
        this.authorDao = authorDao;
        this.genreDao = genreDao;
    }

    @ShellMethod(value = "Добавление новой книги", key = "create-book")
    public String createBook(String name, @ShellOption(defaultValue = "") String annotation, @ShellOption Long authorId, @ShellOption Long genreId) {
        try {
            Author author = Optional.ofNullable(authorDao.find(authorId)).orElseThrow(() -> new EntityNotFoundException(Messages.entityNotFoundError(AUTHOR_ENTITY)));
            Genre genre = Optional.ofNullable(genreDao.find(genreId)).orElseThrow(() -> new EntityNotFoundException(Messages.entityNotFoundError(GENRE_ENTITY)));
            Book book = EntityFactory.createEntity(Book.class);
            book.setName(name);
            book.setAnnotation(annotation);
            book.setGenre(genre);
            book.setAuthor(author);
            long id = bookDao.create(book);
            return Messages.entityCreated("Книга", id);
        } catch (Exception e) {
            return Messages.unexpectedError(e.getMessage());
        }
    }

    @ShellMethod(value = "Обновление информации о книге", key = {"update-book"})
    public String updateBook(Long id,
                             @ShellOption(defaultValue = ShellOption.NULL) String name,
                             @ShellOption(defaultValue = ShellOption.NULL) String annotation,
                             @ShellOption(defaultValue = ShellOption.NULL) Long authorId,
                             @ShellOption(defaultValue = ShellOption.NULL) Long genreId) {
        try {
            Book book = Optional.ofNullable(bookDao.find(id)).orElseThrow(() -> new EntityNotFoundException(Messages.entityNotFoundError(BOOK_ENTITY)));
            book.setName(paramExist(name) ? name : book.getName());
            book.setAnnotation(paramExist(annotation) ? annotation : book.getAnnotation());
            book.setAuthor(Optional.ofNullable(authorDao.find(authorId)).orElseThrow(() -> new EntityNotFoundException(Messages.entityNotFoundError(AUTHOR_ENTITY))));
            book.setGenre(Optional.ofNullable(genreDao.find(genreId)).orElseThrow(() -> new EntityNotFoundException(Messages.entityNotFoundError(GENRE_ENTITY))));
            bookDao.update(book);
            return Messages.entityUpdated("Книга", id);
        } catch (Exception e) {
            return Messages.unexpectedError(e.getMessage());
        }
    }

    @ShellMethod(value = "Ищет книгу по названию или ID", key = "find-book")
    public String findBook(Long id, String name) {
        try {
            Optional<Book> book = Optional.ofNullable(bookDao.find(id));
            if (book.isPresent()) {
                return EntityPrinter.printFull(book.get());
            } else {
                StringBuilder sb = new StringBuilder();
                Optional.ofNullable(bookDao.findByName(name)).ifPresent(list -> {
                    list.forEach(b -> sb.append(EntityPrinter.printFull(b)));
                });
                return sb.toString().isEmpty() ? Messages.entityNotFoundError("Книга") : sb.toString();
            }
        } catch (Exception e) {
            return Messages.unexpectedError(e.getMessage());
        }
    }

    @ShellMethod(value = "Просмотр всех книг", key = "list-book")
    public String findAllBooks() {
        return bookDao.findAll().stream().map(EntityPrinter::printFull).collect(Collectors.joining("\n"));
    }

    @ShellMethod(value = "Удаляет книгу по id", key = "delete-book")
    public String deleteBook(Long id) {
        if (paramExist(id)) {
            bookDao.delete(id);
            return Messages.entityDeleted("Книга", id);
        } else {
            return Messages.emptyParamError("id");
        }
    }

    private boolean paramExist(Object param) {
        return param instanceof String ? !ShellOption.NONE.equals(param) && !ShellOption.NULL.equals(param) : param != null;
    }
}
