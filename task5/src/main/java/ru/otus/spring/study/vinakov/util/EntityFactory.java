package ru.otus.spring.study.vinakov.util;

import ru.otus.spring.study.vinakov.domain.Author;
import ru.otus.spring.study.vinakov.domain.Book;
import ru.otus.spring.study.vinakov.domain.Genre;

import java.util.HashMap;
import java.util.Map;

public class EntityFactory {

    public interface Creator<T> {
        T create();
    }

    private static final Map<Class, Creator> CREATORS = new HashMap<>();
    static {
        registerCreator(Author.class, () -> new Author(Constants.ENTITY_FAKE_ID));
        registerCreator(Genre.class, () -> new Genre(Constants.ENTITY_FAKE_ID));
        registerCreator(Book.class, () -> new Book(Constants.ENTITY_FAKE_ID));
    }

    public static <T> void registerCreator(Class<T> clazz, Creator<T> creator) {
        CREATORS.put(clazz, creator);
    }

    public static <T> T createEntity(Class<T> clazz) {
        if (CREATORS.containsKey(clazz)) {
            return (T) CREATORS.get(clazz).create();
        } else {
            throw new RuntimeException("Unregistered class: " + clazz.getSimpleName());
        }
    }

}
