package ru.otus.spring.study.vinakov.util;

import com.google.gson.JsonObject;
import ru.otus.spring.study.vinakov.domain.Author;
import ru.otus.spring.study.vinakov.domain.Book;
import ru.otus.spring.study.vinakov.domain.Genre;

import static ru.otus.spring.study.vinakov.util.Constants.DATE_FORMAT;

public class EntityPrinter {

    private static class KeyVal {
        private final String key;
        private final String val;

        KeyVal(String key, String val) {
            this.key = key;
            this.val = val;
        }
    }

    public static String printShort(Book book) {
        return "Книга: " + buildBookShort(book).toString();
    }

    public static String printFull(Book book) {
        return "Книга: " + buildBookFull(book).toString();
    }

    public static String printFull(Author author) {
        return "Автор: " + buildAuthor(author);
    }

    public static String printFull(Genre genre) {
        return "Жанр: " + buildGenre(genre);
    }

    private static JsonObject buildBookShort(Book book) {
        return buildObject(
                new KeyVal("ID", String.valueOf(book.getId())),
                new KeyVal("Название", book.getName()),
                new KeyVal("Аннотация", book.getAnnotation()));
    }

    private static JsonObject buildBookFull(Book book) {
        JsonObject jsonBook = buildBookShort(book);
        jsonBook.addProperty("Жанр",buildGenre(book.getGenre()).toString());
        jsonBook.addProperty("Автор", buildAuthor(book.getAuthor()).toString());
        return jsonBook;
    }

    private static JsonObject buildGenre(Genre genre) {
        return buildObject(new KeyVal("ID", String.valueOf(genre.getId())),
                new KeyVal("Название", genre.getName()));
    }

    private static JsonObject buildAuthor(Author author) {
        return buildObject(new KeyVal("ID", String.valueOf(author.getId())),
                new KeyVal("Имя", author.getName()),
                new KeyVal("Дата рождения", DATE_FORMAT.format(author.getBirthday())));
    }

    private static JsonObject buildObject(KeyVal... keyValues) {
        JsonObject jsonObject = new JsonObject();
        if (keyValues != null) {
            for (KeyVal keyVal : keyValues) {
                jsonObject.addProperty(keyVal.key, keyVal.val);
            }
        }
        return jsonObject;
    }

}
