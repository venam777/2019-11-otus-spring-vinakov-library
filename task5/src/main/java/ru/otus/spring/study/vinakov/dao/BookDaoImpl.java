package ru.otus.spring.study.vinakov.dao;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.otus.spring.study.vinakov.domain.Author;
import ru.otus.spring.study.vinakov.domain.Book;
import ru.otus.spring.study.vinakov.domain.Genre;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository
@SuppressWarnings({"SqlNoDataSourceInspection", "SqlDialectInspection"})
public class BookDaoImpl implements BookDao {

    private final NamedParameterJdbcOperations jdbc;

    private static class BookInternalRowMapper implements RowMapper<Book> {

        @Override
        public Book mapRow(ResultSet resultSet, int i) throws SQLException {
            //resultSet.get
            return new Book(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getString("annotation"), new Genre(resultSet.getLong("genre_id")), new Author(resultSet.getLong("author_id")));
        }
    }

    public BookDaoImpl(NamedParameterJdbcOperations jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public long create(Book book) {
        KeyHolder kh = new GeneratedKeyHolder();
        jdbc.update("insert into book(name, annotation, genre_id, author_id) values (:name, :annotation, :genre_id, :author_id)", new MapSqlParameterSource()
                .addValue("name", book.getName())
                .addValue("annotation", book.getAnnotation())
                .addValue("genre_id", book.getGenre().getId())
                .addValue("author_id", book.getAuthor().getId()), kh);
        return kh.getKey().longValue();
    }

    @Override
    public Book find(long id) {
        return jdbc.queryForObject("select b.id id, b.name name, b.annotation annotation, a.id author_id, a.name author_name," +
                " a.birthday author_birthday, g.id genre_id, g.name genre_name from book b left join author a on b.author_id = a.id " +
                "left join genre g on b.genre_id = g.id where b.id=:id", new MapSqlParameterSource()
                .addValue("id", id), new ExternalRowMappers.BookFullRowMapper());
    }

    @Override
    public void update(Book book) {
        jdbc.update("update book set name=:name, annotation=:annotation, genre_id=:genre_id, author_id=:author_id where id=:id", new MapSqlParameterSource()
                .addValue("name", book.getName())
                .addValue("annotation", book.getAnnotation())
                .addValue("genre_id", book.getGenre().getId())
                .addValue("author_id", book.getAuthor().getId())
                .addValue("id", book.getId()));
    }

    @Override
    public void delete(long id) {
        jdbc.update("delete from book where id=:id", Collections.singletonMap("id", id));
    }

    @Override
    public Collection<Book> findAll() {
        return jdbc.query("select b.id id, b.name name, b.annotation annotation, a.id author_id, a.name author_name, " +
                "a.birthday author_birthday, g.id genre_id, g.name genre_name from book b left join author a on b.author_id = a.id " +
                "left join genre g on b.genre_id = g.id", new ExternalRowMappers.BookFullRowMapper());
    }

    @Override
    public List<Book> findByName(String name) {
        return jdbc.query("select b.id id, b.name name, b.annotation annotation, a.id author_id, a.name author_name, " +
                " a.birthday author_birthday, g.id genre_id, g.name genre_name from book b " +
                " left join author a on b.author_id = a.id " +
                " left join genre g on b.genre_id = g.id" +
                " where b.name=:name", new MapSqlParameterSource()
                .addValue("name", name), new ExternalRowMappers.BookFullRowMapper());
    }

    @Override
    public long count() {
        return Optional.ofNullable(jdbc.queryForObject("select count(*) from book", new HashMap<>(), Long.class)).orElse(0L);
    }
}
