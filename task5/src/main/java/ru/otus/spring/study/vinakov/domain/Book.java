package ru.otus.spring.study.vinakov.domain;

public class Book {

    private long id;
    private String name;
    private String annotation;
    private Genre genre;
    private Author author;

    public Book(long id) {
        this.id = id;
    }

    public Book(long id, String name, String annotation) {
        this.id = id;
        this.name = name;
        this.annotation = annotation;
    }

    public Book(long id, String name, String annotation, Genre genre, Author author) {
        this.id = id;
        this.name = name;
        this.annotation = annotation;
        this.genre = genre;
        this.author = author;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }
}
