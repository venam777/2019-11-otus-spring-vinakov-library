package ru.otus.spring.study.vinakov.dao;

import org.springframework.jdbc.core.RowMapper;
import ru.otus.spring.study.vinakov.domain.Author;
import ru.otus.spring.study.vinakov.domain.Book;
import ru.otus.spring.study.vinakov.domain.Genre;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ExternalRowMappers {

    public static class AuthorExternalRowMapper implements RowMapper<Author> {

        @Override
        public Author mapRow(ResultSet resultSet, int i) throws SQLException {
            return new Author(resultSet.getInt("author_id"), resultSet.getString("author_name"), resultSet.getDate("author_birthday"));
        }
    }

    public static class GenreExternalRowMapper implements RowMapper<Genre> {

        @Override
        public Genre mapRow(ResultSet resultSet, int i) throws SQLException {
            return new Genre(resultSet.getLong("genre_id"), resultSet.getString("genre_name"));
        }
    }

    public static class BookFullRowMapper implements RowMapper<Book> {

        @Override
        public Book mapRow(ResultSet resultSet, int i) throws SQLException {
            return new Book(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getString("annotation"),
                    new GenreExternalRowMapper().mapRow(resultSet,i),
                    new AuthorExternalRowMapper().mapRow(resultSet, i));
        }
    }

}
