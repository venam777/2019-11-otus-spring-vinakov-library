package ru.otus.spring.study.vinakov.dao;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.otus.spring.study.vinakov.domain.Author;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.*;

@Repository
@SuppressWarnings({"SqlNoDataSourceInspection", "SqlDialectInspection"})
public class AuthorDaoImpl implements AuthorDao {

    private static class AuthorInternalRowMapper implements RowMapper<Author> {

        @Override
        public Author mapRow(ResultSet resultSet, int i) throws SQLException {
            return new Author(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getDate("birthday"));
        }
    }

    private final NamedParameterJdbcOperations jdbc;

    public AuthorDaoImpl(NamedParameterJdbcOperations jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public long create(Author author) {
        KeyHolder kh = new GeneratedKeyHolder();
        jdbc.update("insert into author (name, birthday) values (:name, :birthday)",
                new MapSqlParameterSource()
                        .addValue("name", author.getName())
                        .addValue("birthday", author.getBirthday() != null ? new java.sql.Date(author.getBirthday().getTime()) : null, Types.DATE), kh);
        return kh.getKey().longValue();
    }

    @Override
    public Author find(long id) {
        return jdbc.queryForObject("select * from author where id = :id", Collections.singletonMap("id", id), new AuthorInternalRowMapper());
    }

    @Override
    public void update(Author author) {
        jdbc.update("update author set name=:name, birthday=:birthday where id=:id", new MapSqlParameterSource()
                .addValue("id", author.getId())
                .addValue("name", author.getName())
                .addValue("birthday", author.getBirthday() != null ? new java.sql.Date(author.getBirthday().getTime()) : null));
    }

    @Override
    public void delete(long id) {
        jdbc.update("delete from author where id = :id", Collections.singletonMap("id", id));
    }

    @Override
    public Collection<Author> findAll() {
        return jdbc.query("select * from author", new AuthorInternalRowMapper());
    }

    @Override
    public List<Author> findByName(String name) {
        return jdbc.query("select * from author where name=:name", new MapSqlParameterSource()
                .addValue("name", name), new AuthorInternalRowMapper());
    }

    @Override
    public long count() {
        return Optional.ofNullable(jdbc.queryForObject("select count(*) from author", new HashMap<>(), Long.class)).orElse(0L);
    }
}
