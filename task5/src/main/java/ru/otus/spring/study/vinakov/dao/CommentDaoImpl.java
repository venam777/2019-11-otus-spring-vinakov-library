package ru.otus.spring.study.vinakov.dao;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.stereotype.Repository;

@Repository
public class CommentDaoImpl implements CommentDao {

    private final NamedParameterJdbcOperations jdbc;

    public CommentDaoImpl(NamedParameterJdbcOperations jdbc) {
        this.jdbc = jdbc;
    }
}
