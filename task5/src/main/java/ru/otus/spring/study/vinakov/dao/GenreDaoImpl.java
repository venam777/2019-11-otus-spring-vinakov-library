package ru.otus.spring.study.vinakov.dao;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.otus.spring.study.vinakov.domain.Genre;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
@SuppressWarnings({"SqlNoDataSourceInspection", "SqlDialectInspection"})
public class GenreDaoImpl implements GenreDao {

    private static class GenreInternalRowMapper implements RowMapper<Genre> {

        @Override
        public Genre mapRow(ResultSet resultSet, int i) throws SQLException {
            return new Genre(resultSet.getLong("id"), resultSet.getString("name"));
        }
    }

    private final NamedParameterJdbcOperations jdbc;

    public GenreDaoImpl(NamedParameterJdbcOperations jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public long create(Genre genre) {
        KeyHolder kh = new GeneratedKeyHolder();
        jdbc.update("insert into genre(name) values (:name)", new MapSqlParameterSource()
                .addValue("name", genre.getName()), kh);
        return kh.getKey().longValue();
    }

    @Override
    public Genre find(long id) {
        return jdbc.queryForObject("select id, name from genre where id=:id", new MapSqlParameterSource()
                .addValue("id", id), new GenreInternalRowMapper());
    }

    @Override
    public void update(Genre genre) {
        jdbc.update("update genre set name=:name where id=:id", new MapSqlParameterSource()
                .addValue("name", genre.getName())
                .addValue("id", genre.getId()));
    }

    @Override
    public void delete(long id) {
        jdbc.update("delete from genre where id=:id", new MapSqlParameterSource()
                .addValue("id", id));
    }

    @Override
    public Collection<Genre> findAll() {
        return jdbc.query("select * from genre", new GenreInternalRowMapper());
    }

    @Override
    public long count() {
        return Optional.ofNullable(jdbc.queryForObject("select count(*) from genre", new HashMap<>(), Long.class)).orElse(0L);
    }

    @Override
    public List<Genre> findByName(String name) {
        return jdbc.query("select id,name from genre where name=:name",
                new MapSqlParameterSource().addValue("name", name),
                new GenreInternalRowMapper());
    }
}
