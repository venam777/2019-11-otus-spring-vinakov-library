package ru.otus.spring.study.vinakov.repository;

import ru.otus.spring.study.vinakov.domain.Comment;
import ru.otus.spring.study.vinakov.dto.BookRating;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@SuppressWarnings("unused")
public class CommentRepositoryCustomImpl implements CommentRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Comment> findLastCommentsByBook(long bookId, int count) {
        return entityManager.createQuery("select c from Comment c where c.book.id = :bookId", Comment.class)
                .setParameter("bookId", bookId)
                .setMaxResults(count)
                .getResultList();
    }

    @Override
    public List<BookRating> findTopRatingBooks(int count) {
        return entityManager.createQuery("select new ru.otus.spring.study.vinakov.dto.BookRating( c.book, avg(c.rating))" +
                " from Comment c group by c.book order by avg(c.rating) desc", BookRating.class).setMaxResults(count).getResultList();
    }

}
