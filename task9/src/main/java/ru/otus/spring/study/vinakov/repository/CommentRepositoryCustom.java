package ru.otus.spring.study.vinakov.repository;

import ru.otus.spring.study.vinakov.domain.Comment;
import ru.otus.spring.study.vinakov.dto.BookRating;

import java.util.List;

public interface CommentRepositoryCustom {

    List<Comment> findLastCommentsByBook(long bookId, int count);

    List<BookRating> findTopRatingBooks(int count);

}
