package ru.otus.spring.study.vinakov.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.otus.spring.study.vinakov.repository.GenreRepository;

@Controller
public class GenreController {

    private final GenreRepository genreRepository;

    public GenreController(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }

    @GetMapping("/genre/list")
    public String viewAll(Model model) {
        model.addAttribute("genres", genreRepository.findAll());
        return "/genre/view-all";
    }

}
