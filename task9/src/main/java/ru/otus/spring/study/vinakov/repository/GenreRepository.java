package ru.otus.spring.study.vinakov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.otus.spring.study.vinakov.domain.Genre;

import java.util.List;

@SuppressWarnings("unused")
public interface GenreRepository extends JpaRepository<Genre, Long> {

    List<Genre> findByName(String name);

}
