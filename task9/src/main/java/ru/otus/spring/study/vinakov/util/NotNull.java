package ru.otus.spring.study.vinakov.util;

public class NotNull<T> {

    private final T value;

    public NotNull(T value, T ifNull) {
        this.value = value != null ? value : ifNull;
    }

    public T getValue() {
        return value;
    }
}
