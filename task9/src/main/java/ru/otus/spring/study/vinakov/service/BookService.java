package ru.otus.spring.study.vinakov.service;

import org.springframework.transaction.annotation.Transactional;

public interface BookService {

    //я бы сделал это каскадными операциями, но Вы в одном из прошлых ДЗ говорили, что комменты не должны быть частью книги.
    //так что приходится выкручиваться так
    @Transactional
    void delete(long bookId);
}
