package ru.otus.spring.study.vinakov.dto;

import ru.otus.spring.study.vinakov.domain.Comment;
import ru.otus.spring.study.vinakov.util.Constants;

import java.util.Date;

public class CommentDto {

    private long id;
    private String text;
    private Date created;
    private String createdString;
    private int rating;
    private BookDto book;

    public CommentDto() {
    }

    public CommentDto(long id, String text, Date created, String createdString, int rating, BookDto book) {
        this.id = id;
        this.text = text;
        this.created = created;
        this.createdString = createdString;
        this.rating = rating;
        this.book = book;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getCreatedString() {
        return createdString;
    }

    public void setCreatedString(String createdString) {
        this.createdString = createdString;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public BookDto getBook() {
        return book;
    }

    public void setBook(BookDto book) {
        this.book = book;
    }

    public static CommentDto fromEntity(Comment c) {
        return new CommentDto(c.getId(), c.getText(), c.getCreated(), Constants.DATE_FORMAT.format(c.getCreated()),
                c.getRating(), BookDto.fromEntity(c.getBook()));
    }
}
