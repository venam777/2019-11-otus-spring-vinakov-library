package ru.otus.spring.study.vinakov.controller;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.otus.spring.study.vinakov.service.BookCommentServiceImpl;
import ru.otus.spring.study.vinakov.service.BookServiceImpl;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//таким способом не заводится, пишет ошибку org.springframework.beans.factory.NoSuchBeanDefinitionException: No bean named 'entityManagerFactory' available
//работающее решение нашел тут( причем, ответ, помеченный как правильный, так же не работает): https://issue.life/questions/48078044
//но, наверное, это не лучший способ решить эту проблему?
//@WebMvcTest(BookController.class)
//@EnableJpaRepositories(basePackages = "ru.otus.spring.study.vinakov.repository")
//@Import({BookServiceImpl.class, BookCommentServiceImpl.class})
@AutoConfigureMockMvc
@SpringBootTest
public class BookControllerTest {

    @Autowired
    private MockMvc controller;

    @Test
    public void testListAll() throws Exception {
        String result = controller.perform(MockMvcRequestBuilders.get("/book/list")
        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertTrue(result.contains("Евгений Онегин"));
        assertTrue(result.contains("Мцыри"));
    }

}
