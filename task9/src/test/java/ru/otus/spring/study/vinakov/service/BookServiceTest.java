package ru.otus.spring.study.vinakov.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import ru.otus.spring.study.vinakov.domain.Author;
import ru.otus.spring.study.vinakov.domain.Book;
import ru.otus.spring.study.vinakov.domain.Comment;
import ru.otus.spring.study.vinakov.domain.Genre;

import javax.persistence.TypedQuery;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@Import(BookServiceImpl.class)
public class BookServiceTest {

    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private BookServiceImpl bookService;

    @Test
    public void testDeleteBook() {
        Book book = createBook();
        entityManager.persist(book);
        entityManager.persist(createComment(book));
        entityManager.persist(createComment(book));
        entityManager.persist(createComment(book));

        TypedQuery<Comment> query = entityManager.getEntityManager().createQuery("select c from Comment c where c.book.id = :bookId", Comment.class);
        query.setParameter("bookId", book.getId());
        assertEquals(query.getResultList().size(), 3);
        bookService.delete(book.getId());
        assertEquals(query.getResultList().size(), 0);
    }

    private Book createBook() {
        Book book = new Book();
        book.setName("Test");
        book.setAnnotation("fdfsgfd");
        book.setAuthor(entityManager.find(Author.class, 1L));
        book.setGenre(entityManager.find(Genre.class, 1L));
        return book;
    }

    private Comment createComment(Book book) {
        Comment c = new Comment();
        c.setBook(book);
        c.setCreated(new Date());
        c.setText("Text");
        c.setRating(5);
        return c;
    }

}
