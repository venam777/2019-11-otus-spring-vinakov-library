let EVENT_SOURCE;
function listBooks(containerId) {
    $.ajax({
        url: "/rest/api/book/list",
        type: 'GET',
        success: function (books) {
            let container = $("#" + containerId);
            $(container).empty();
            var tbody = "";
            books.forEach(function (book) {
                tbody += ` <tr>
                                    <td><a href="#" onclick="viewBook('${containerId}', '${book.id}')">${book.name}</a>
                                    <td>${book.annotation}</td>
                                    <td>${book.genre.name}</td>
                                    <td>${book.author.name}</td>
                                    <td><i class="far fa-edit" style="cursor: pointer" onclick="editBook('${book.id}', () => listBooks('${containerId}'))"></i></td>
                                    <td><i class="far fa-trash-alt" style="cursor: pointer" onclick="deleteBook('${book.id}', true, () => listBooks('${containerId}'))"></i></td>
                                    <td></td>
                                </tr>`;
            });
            $(container).append(`
                     <table class="pure-table pure-table-horizontal">
                        <thead>
                            <tr>
                                <th>Название</th>
                                <th>Описание</th>
                                <th>Жанр</th>
                                <th>Автор</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            ${tbody}                        
                        </tbody>
                </table>`);
            $(container).append(`<button class="pure-button pure-button-primary" style="float: right; margin: 30px 50px 0 0" onclick="createBook()">Добавить книгу</button>`)
        }
    });
}

function viewBook(containerId, bookId) {
    let container = $("#" + containerId);
    $(container).empty();
    $.ajax({
        url: `/rest/api/book/${bookId}`,
        type: "GET",
        success: (book) => {
            $(container).append(`
<div>
    <form class="pure-form pure-form-aligned">
        <fieldset>
            <div class="pure-control-group">
                <label for="name">Название</label>
                <span id="name">${book.name}</span>
            </div>
            <div class="pure-control-group">
                <label for="annotation" style="vertical-align: top">Описание</label>
                <span id="annotation" style="display: inline-block; max-width: 600px">${book.annotation}</span>
            </div>
             <div class="pure-control-group">
                <label for="author">Автор</label>
                <span id="author">${book.author.name}</span>
            </div>
             <div class="pure-control-group">
                <label for="genre">Жанр</label>
                <span id="genre">${book.genre.name}</span>
            </div>
             <div class="pure-control-group">
                <label for="rating">Рейтинг</label>
                <span id="rating">${book.rating}</span>
            </div>
        </fieldset>
    </form>
</div>
<div>
    <div id="book-comments-container" style="width: 500px">
    <ul></ul>
</div>
    <button class="pure-button pure-button-primary" id="create-comment-button">Добавить комментарий</button>
</div>`);
            loadComments(bookId);
            // loadBookComments(bookId);
            //если прописать onclick в Html - он не работает, причем, только в этом месте. черная магия...пришлось выкручиваться js-ом
            // onclick="createComment(${bookId}, () => loadComments(${bookId}))"
            $("#create-comment-button").click(() => createComment(bookId, () => loadComments(bookId)));
        }
    });

}

function loadComments(bookId) {
    $.ajax({
        url: `/rest/api/book/${bookId}/comments`,
        type: "GET",
        success: (comments) => {
            var str = ``;
            if (comments) {
                comments.forEach((c) => {
                    str += `
                    <li>
                        <span>Оценка: ${c.rating}</span>
                        <span style="float: right;">${c.createdString}</span>
                        <div>${c.text}</div>
                    </li>`;
                });
            }
            let container = $('#book-comments-container').find("ul");
            $(container).empty();
            $(container).append(str)
        }
    })
}

function editBook(bookId) {
    location.href = `/book/edit/${bookId}`;
}

function createBook() {
    location.href = "/book/create";
}

function createComment(bookId, callback) {
    var dialog = $("#create-comment-container").dialog({
        autoOpen: false,
         // height: 400,
         width: 500,
        modal: true,
        title: "Добавить комментарий",
        buttons: {
            "Сохранить": function() {
                $(dialog).dialog("close");
                $("#create-comment-form").ajaxSubmit({
                    url: `/rest/api/book/${bookId}/comment`,
                    type: 'POST',
                    dataType: "application/json",
                    success: callback});
            },
            "Отмена": function () {
                $(dialog).dialog("close");
            }
        }
    });
    $(dialog).find("input[name='book']").val(bookId);
    $(dialog).dialog("open");
}

function deleteBook(bookId, needConfirm, callback) {
    if (!needConfirm || confirm("Вы действительно хотите удалить книгу?")) {
        $.ajax({
            url: "/rest/api/book/" + bookId,
            type: "DELETE",
            success: callback,
            error: () => alert("Во время удаления книги произошла ошибка")
        })
    }
}

/*
 Попытка использовать SSE на клиенте, которая не увенчалась успехом: EventSource висит в постоянном реконнекте (подробнее в BookRestController)
 */
function loadBookComments(bookId) {
    if (EVENT_SOURCE) {
        EVENT_SOURCE.close();
    }
    EVENT_SOURCE = new EventSource("/rest/api/book/" + bookId + "/comments");
    EVENT_SOURCE.onmessage = (event) => {
        console.log("event: " + event);
        let c = JSON.parse(event.data);
        let container = $('#book-comments-container').find("ul");
        $(container).empty();
        $(container).append(` <li>
                        <span>Оценка: ${c.rating}</span>
                        <span style="float: right;">${c.createdString}</span>
                        <div>${c.text}</div>
                    </li>`)
    };
    EVENT_SOURCE.ope = () => console.log("ERROR");
}