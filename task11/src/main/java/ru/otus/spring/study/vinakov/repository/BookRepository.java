package ru.otus.spring.study.vinakov.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.otus.spring.study.vinakov.domain.Book;

public interface BookRepository extends ReactiveMongoRepository<Book, String> {

    Flux<Book> findByName(String name);

    Mono<Void> deleteAllByAuthorId(String authorId);

    Mono<Void> deleteAllByGenreId(String genreId);
}
