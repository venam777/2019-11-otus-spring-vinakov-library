package ru.otus.spring.study.vinakov.repository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.otus.spring.study.vinakov.domain.Comment;
import ru.otus.spring.study.vinakov.dto.BookRating;

public interface CommentRepositoryCustom {

    Flux<Comment> findLastCommentsByBook(String bookId, int count);

    Flux<BookRating> findTopRatingBooks(int count);

    Mono<Double> getBookRating(String bookId);

}
