package ru.otus.spring.study.vinakov;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@SpringBootApplication
@EnableReactiveMongoRepositories(basePackages = "ru.otus.spring.study.vinakov.repository")
public class Main {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Main.class);
    }
}
