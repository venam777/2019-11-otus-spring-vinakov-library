package ru.otus.spring.study.vinakov.util;

public class Messages {

    private static final String PARAMETER_IS_EMPTY_ERROR = "Параметр %s должен быть заполнен";
    private static final String PARAMETER_INVALID_FORMAT = "Параметр %s заполнен неправильно";
    private static final String ENTITY_NOT_FOUND_ERROR = "Объект '%s' не найден";
    private static final String TO_MANY_ENTITIES_ERROR = "По заданным критериям найдено слишком много объектов '%s': %d";
    private static final String UNEXPECTED_ERROR = "Произошла непредвиденная ошибка: %s";

    private static final String ENTITY_CREATED = "Объект '%s' с ID %d успешно создан";
    private static final String ENTITY_UPDATED = "Объект '%s' с ID %d успешно обновлен";
    private static final String ENTITY_DELETED = "Объект '%s' с ID %d успешно удален";

    public static String emptyParamError(String paramName) {
        return String.format(PARAMETER_IS_EMPTY_ERROR, paramName);
    }

    public static String parameterInvalidFormat(String param) {
        return String.format(PARAMETER_INVALID_FORMAT, param);
    }

    public static String entityNotFoundError(String entity) {
        return String.format(ENTITY_NOT_FOUND_ERROR, entity);
    }

    public static String toManyEntitiesError(String entity, Integer count) {
        return String.format(TO_MANY_ENTITIES_ERROR, entity, count);
    }

    public static String unexpectedError(String message) {
        return String.format(UNEXPECTED_ERROR, message);
    }

    public static String entityCreated(String entity, Long id) {
        return String.format(ENTITY_CREATED, entity, id);
    }

    public static String entityUpdated(String entity, Long id) {
        return String.format(ENTITY_UPDATED, entity, id);
    }

    public static String entityDeleted(String entity, Long id) {
        return String.format(ENTITY_DELETED, entity, id);
    }
}
