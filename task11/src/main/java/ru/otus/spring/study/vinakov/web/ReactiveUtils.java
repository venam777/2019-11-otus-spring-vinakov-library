package ru.otus.spring.study.vinakov.web;

import org.thymeleaf.spring5.context.webflux.IReactiveDataDriverContextVariable;
import org.thymeleaf.spring5.context.webflux.ReactiveDataDriverContextVariable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class ReactiveUtils {

    public static IReactiveDataDriverContextVariable forModel(Flux flux) {
        return new ReactiveDataDriverContextVariable(flux);
    }

    public static IReactiveDataDriverContextVariable forModel(Mono mono) {
        return new ReactiveDataDriverContextVariable(mono);
    }

}
