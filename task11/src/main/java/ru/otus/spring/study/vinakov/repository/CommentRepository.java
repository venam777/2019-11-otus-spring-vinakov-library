package ru.otus.spring.study.vinakov.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.otus.spring.study.vinakov.domain.Comment;

public interface CommentRepository extends ReactiveMongoRepository<Comment, String>, CommentRepositoryCustom {

    Flux<Comment> findByBookId(String bookId);

    Flux<Comment> findByBookIdAndRating(String bookId, int rating);

    Mono<Void> deleteAllByBookId(String bookId);

}