package ru.otus.spring.study.vinakov.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;
import ru.otus.spring.study.vinakov.domain.Genre;

@SuppressWarnings("unused")
public interface GenreRepository extends ReactiveMongoRepository<Genre, String> {

    Flux<Genre> findByName(String name);

}
