package ru.otus.spring.study.vinakov.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.result.view.RedirectView;
import reactor.core.publisher.Mono;
import ru.otus.spring.study.vinakov.domain.Author;
import ru.otus.spring.study.vinakov.domain.Book;
import ru.otus.spring.study.vinakov.domain.Genre;
import ru.otus.spring.study.vinakov.dto.BookDto;
import ru.otus.spring.study.vinakov.dto.BookRequestBody;
import ru.otus.spring.study.vinakov.dto.CommentDto;
import ru.otus.spring.study.vinakov.exception.EntityNotFoundException;
import ru.otus.spring.study.vinakov.repository.AuthorRepository;
import ru.otus.spring.study.vinakov.repository.BookRepository;
import ru.otus.spring.study.vinakov.repository.CommentRepository;
import ru.otus.spring.study.vinakov.repository.GenreRepository;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Controller
public class BookController {

    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;
    private final GenreRepository genreRepository;
    private final CommentRepository commentRepository;

    public BookController(BookRepository bookRepository, AuthorRepository authorRepository,
                          GenreRepository genreRepository, CommentRepository commentRepository) {
        this.bookRepository = bookRepository;
        this.authorRepository = authorRepository;
        this.genreRepository = genreRepository;
        this.commentRepository = commentRepository;
    }

    @GetMapping("/book/list")
    public String viewAll(Model model) {
        model.addAttribute("books", ReactiveUtils.forModel(bookRepository.findAll()));
        return "book/view-all";
    }

    @GetMapping("/book/edit/{id}")
    public String viewBook(@PathVariable("id") String id, Model model) {
        Book book = bookRepository.findById(id).switchIfEmpty(Mono.error(EntityNotFoundException::new)).block();
        model.addAttribute("authors", getSorted(authorRepository.findAll().toStream().collect(Collectors.toList()), Comparator.comparing(Author::getName)));
        model.addAttribute("genres", getSorted(genreRepository.findAll().toStream().collect(Collectors.toList()), Comparator.comparing(Genre::getName)));
        model.addAttribute("book", book);
        return "book/edit";
    }

    @GetMapping("/book/create")
    public String createBook(Model model) {
        model.addAttribute("book", new Book());
        //List<Author> authors = getSorted(ReactiveUtils.from(authorRepository.findAll().collectList().block()), Comparator.comparing(Author::getName));
        model.addAttribute("authors", getSorted(Objects.requireNonNull(authorRepository.findAll().collectList().block()), Comparator.comparing(Author::getName)));
        model.addAttribute("genres", getSorted(Objects.requireNonNull(genreRepository.findAll().collectList().block()), Comparator.comparing(Genre::getName)));
//        List<Genre> genres = getSorted(ReactiveUtils.from(genreRepository.findAll()), Comparator.comparing(Genre::getName));
//        model.addAttribute("genres", genres);
        return "book/edit";
    }

    @GetMapping("/book/details/{id}")
    public String view(@PathVariable("id") String id, Model model) {
        Book book = bookRepository.findById(id).switchIfEmpty(Mono.error(EntityNotFoundException::new)).block();
        BookDto bookDto = BookDto.fromEntity(book);
        List<CommentDto> comments = commentRepository.findByBookId(id).toStream().map(CommentDto::fromEntity).collect(Collectors.toList());
        bookDto.setRating(comments.stream().mapToDouble(CommentDto::getRating).average().orElse(0));
        model.addAttribute("book", bookDto);
        model.addAttribute("comments", comments);
        model.addAttribute("rating", String.format("%.1f", bookDto.getRating()));
        return "/book/view";

    }

    @PostMapping("/book/save")
    public RedirectView save(BookRequestBody book) {
        Book b = new Book(book.getId() != "" ? book.getId() : null);
        b.setName(book.getName());
        b.setAnnotation(book.getAnnotation());
        b.setGenre(genreRepository.findById(book.getGenre()).block());
        b.setAuthor(authorRepository.findById(book.getAuthor()).block());
        Book result = bookRepository.save(b).block();
        BookDto dto = BookDto.fromEntity(result);
        return new RedirectView("/book/details/" + dto.getId());
    }

    //на delete не получилось переделать, спринг выдает: Resolved [org.springframework.web.HttpRequestMethodNotSupportedException: Request method 'DELETE' not supported]
    @GetMapping("/delete/{id}")
    public RedirectView deleteBook(@PathVariable("id") String id) {
        commentRepository.deleteAllByBookId(id).subscribe();
        bookRepository.deleteById(id).subscribe();
        return new RedirectView("/book/list");
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public String notFoundHandler(EntityNotFoundException e, Model model) {
        model.addAttribute("message", "Книга не найдена");
        return "not-found";
    }

    private <T> List<T> getSorted(List<T> entities, Comparator<T> comparator) {
        entities.sort(comparator);
        return entities;
    }

}
