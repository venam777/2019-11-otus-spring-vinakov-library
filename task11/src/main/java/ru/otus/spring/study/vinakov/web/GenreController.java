package ru.otus.spring.study.vinakov.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.otus.spring.study.vinakov.dto.GenreDto;
import ru.otus.spring.study.vinakov.repository.GenreRepository;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class GenreController {

    private final GenreRepository genreRepository;

    public GenreController(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }

    @GetMapping("/genre/list")
    public String viewAll(Model model) {
        List<GenreDto> genres = genreRepository.findAll().map(GenreDto::fromEntity).toStream().collect(Collectors.toList());
        model.addAttribute("genres", genres);
        return "genre/view-all";
    }

}
