package ru.otus.spring.study.vinakov.dto;

/**
 * этот класс пришлось добавлять так как thymeleaf не умеет сохранять\передавать на сервер объекты в данном ДЗ
 * через th:field с формы на контроллер. ругается на несоответствие типов (притом, что объекты простейшие, и в Data JPA это работало)
 */
public class BookRequestBody {

    private String id;
    private String name;
    private String annotation;
    private String genre;
    private String author;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
