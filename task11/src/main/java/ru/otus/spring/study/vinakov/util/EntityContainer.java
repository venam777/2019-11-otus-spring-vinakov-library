package ru.otus.spring.study.vinakov.util;

public class EntityContainer<T> {

    private T entity;

    public T getEntity() {
        return entity;
    }

    public void setEntity(T entity) {
        this.entity = entity;
    }
}
