package ru.otus.spring.study.vinakov.web.rest;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import ru.otus.spring.study.vinakov.dto.AuthorDto;
import ru.otus.spring.study.vinakov.repository.AuthorRepository;

@RestController
public class AuthorRestController {

    private final AuthorRepository authorRepository;

    public AuthorRestController(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @GetMapping(value = "/rest/api/author")
    public Flux<AuthorDto> getAll() {
        return authorRepository.findAll().map(AuthorDto::fromEntity);
    }


}
