package ru.otus.spring.study.vinakov.repository;

import org.bson.Document;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.otus.spring.study.vinakov.domain.Comment;
import ru.otus.spring.study.vinakov.dto.BookRating;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;
import static org.springframework.data.mongodb.core.aggregation.ObjectOperators.ObjectToArray.valueOfToArray;

@SuppressWarnings("unused")
public class CommentRepositoryCustomImpl implements CommentRepositoryCustom {

    private ReactiveMongoTemplate mongoTemplate;

    public CommentRepositoryCustomImpl(ReactiveMongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public Flux<Comment> findLastCommentsByBook(String bookId, int count) {
        Query query = new Query();
        query.addCriteria(Criteria.where("book.id").is(bookId));
        return mongoTemplate.find(new Query().addCriteria(Criteria.where("book.id").is(bookId)).limit(count), Comment.class);
    }

    @Override
    public Flux<BookRating> findTopRatingBooks(int count) {
        return mongoTemplate.aggregate(Aggregation.newAggregation(
                group("book").avg("rating").as("rating")
                ,sort(Sort.Direction.DESC, "rating")
                ,limit(count)
                ,project("rating").and("_id").as("book_id")
                ,project("rating").and(valueOfToArray("book_id")).as("book_map")
                ,project("rating").and("book_map").arrayElementAt(1).as("book_id_map")
                ,project("rating").and("book_id_map.v").as("b_id")
                ,lookup("book", "b_id", "_id", "book")
        ), Comment.class, BookRating.class);
    }

    @Override
    public Mono<Double> getBookRating(String bookId) {
        return mongoTemplate.aggregate(Aggregation.newAggregation(
                match(Criteria.where("book.id").is(bookId)),
                group("book.id").avg("rating").as("rating"),
                Aggregation.project("rating").andExclude("_id")), Comment.class, Document.class)
                .next().map(doc -> doc != null ? (Double) doc.get("rating") : 0d);
    }
}
