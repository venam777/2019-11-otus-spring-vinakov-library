package ru.otus.spring.study.vinakov.dto;

import ru.otus.spring.study.vinakov.domain.Book;

public class BookRating {

    private final Book book;
    private final double rating;

    public BookRating(Book book, double rating) {
        this.book = book;
        this.rating = rating;
    }

    public Book getBook() {
        return book;
    }

    public double getRating() {
        return rating;
    }
}
