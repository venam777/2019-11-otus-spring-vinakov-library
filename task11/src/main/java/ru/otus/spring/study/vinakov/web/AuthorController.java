package ru.otus.spring.study.vinakov.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.otus.spring.study.vinakov.dto.AuthorDto;
import ru.otus.spring.study.vinakov.repository.AuthorRepository;

import java.util.stream.Collectors;

@Controller
public class AuthorController {

    private final AuthorRepository authorRepository;

    public AuthorController(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @GetMapping("/author/list")
    public String viewAll(Model model) {
        //пример с использованием ReactiveDataDriverContextVariable, взят с проекта mkyong WebFlux + Thymeleaf
        //в отдельно скачанном проекте это все работает замечательно, но в моем проекте это не работает ни в какую
        //я уже дошел до того, что взял код с проекта mkyong и вставил его в этот проект, и страница сломалась.
        //я не могу найти причину этого, поэтому пришлось сделать в синхронном режиме
        //буду очень благодарен, если подскажете, в чем причина

//        IReactiveDataDriverContextVariable reactiveDataDrivenMode =
//                new ReactiveDataDriverContextVariable(authorRepository.findAll(), 1);
        model.addAttribute("authors", ReactiveUtils.forModel(authorRepository.findAll()));
        return "author/view-all";
//        model.addAttribute("authors", authorRepository.findAll().map(AuthorDto::fromEntity).toStream().collect(Collectors.toList()));
        //model.addAttribute("authors", ReactiveUtils.from(authorRepository.findAll().map(AuthorDto::fromEntity)));
//        return "author/view-all";
    }
}
