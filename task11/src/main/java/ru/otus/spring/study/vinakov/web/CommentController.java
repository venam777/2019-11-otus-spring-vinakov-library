package ru.otus.spring.study.vinakov.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.reactive.result.view.RedirectView;
import ru.otus.spring.study.vinakov.domain.Book;
import ru.otus.spring.study.vinakov.domain.Comment;
import ru.otus.spring.study.vinakov.exception.EntityNotFoundException;
import ru.otus.spring.study.vinakov.repository.BookRepository;
import ru.otus.spring.study.vinakov.repository.CommentRepository;

import java.util.Date;

@Controller
public class CommentController {

    private final CommentRepository commentRepository;
    private final BookRepository bookRepository;

    public CommentController(CommentRepository commentRepository, BookRepository bookRepository) {
        this.commentRepository = commentRepository;
        this.bookRepository = bookRepository;
    }

    @GetMapping("/comment/create")
    public String create(@RequestParam("book") String bookId, Model model) throws EntityNotFoundException {
        Book book = bookRepository.findById(bookId).block();
        Comment comment = new Comment();
        comment.setBook(book);
        model.addAttribute("comment", comment);
        return "comment/edit";
    }

    @PostMapping("/comment/save")
    public RedirectView save(Comment comment) {
        comment.setCreated(new Date());
        return new RedirectView("/book/" + commentRepository.save(comment).block().getBook().getId());
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public String notFoundHandler(EntityNotFoundException e, Model model) {
        model.addAttribute("message", "Книга не найдена");
        return "/not-found";
    }
}
