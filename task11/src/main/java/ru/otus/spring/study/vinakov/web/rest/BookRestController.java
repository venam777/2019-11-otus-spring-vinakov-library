package ru.otus.spring.study.vinakov.web.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.otus.spring.study.vinakov.domain.Comment;
import ru.otus.spring.study.vinakov.dto.BookDto;
import ru.otus.spring.study.vinakov.dto.CommentDto;
import ru.otus.spring.study.vinakov.exception.EntityNotFoundException;
import ru.otus.spring.study.vinakov.repository.BookRepository;
import ru.otus.spring.study.vinakov.repository.CommentRepository;

import java.util.Date;

@RestController
@SuppressWarnings("unchecked")
public class BookRestController {

    private final BookRepository bookRepository;
    private final CommentRepository commentRepository;

    public BookRestController(BookRepository bookRepository, CommentRepository commentRepository) {
        this.bookRepository = bookRepository;
        this.commentRepository = commentRepository;
    }

    @GetMapping("/rest/api/book/list")
    public Flux<BookDto> getAll() {
        return bookRepository.findAll().map(BookDto::fromEntity);
    }

    @GetMapping("/rest/api/book/{id}")
    public Mono<BookDto> getBook(@PathVariable("id") String bookId) {
        Mono<BookDto> bookDtoMono = bookRepository.findById(bookId).map(BookDto::fromEntity);
        Mono<Double> ratingMono = commentRepository.getBookRating(bookId).defaultIfEmpty(0d);
        return bookDtoMono.zipWith(ratingMono, (book, r) -> {
            book.setRating((double) ((int) (r * 10)) / 10d);
            return book;
        }).switchIfEmpty(Mono.error(new EntityNotFoundException("Книга с id " + bookId + " не найдена")));
    }

    @DeleteMapping("/rest/api/book/{id}")
    public Mono<Void> delete(@PathVariable("id") String bookId) {
        return commentRepository.deleteAllByBookId(bookId).then(bookRepository.deleteById(bookId))
                .then();
    }

    /*
       я пытался сделать этот метод с помощью produces = MediaType.TEXT_EVENT_STREAM_VALUE и возвращать Flux<CommentDto>
       Идея была в том, что при открытой странице книги комментарии подтягивались динамически
       Но проблема в том, что при использовании Server-Sent-Event на клиенте EventSource вместо того, чтобы
       подключиться к серверу и вытаскивать данные по мере их поступления (как описывается это во всех туториалах),
       постоянно висит в состоянии OPENING и постоянно коннектится к серверу, и получает только последний элемент из потока,
       что в принципе логично, учитывая горячий Flux.
       Я пересмотрел, наверное, все доступные туториалы про Server-Sent-Event, но не нашел рабочего примера, как заставить
       клиент подключиться один раз и слушать данные с сервера, поэтому после добавления комментария приходится обновлять страницу книги
       Буду благодарен, если подскажете, в чем причина и как исправить ошибку
     */
    @GetMapping(value = "/rest/api/book/{id}/comments")
    public Flux<CommentDto> getComments(@PathVariable("id") String bookId) {
        return commentRepository.findByBookId(bookId).map(CommentDto::fromEntity);
                /*.map(dto -> ServerSentEvent.<CommentDto>builder()
                        .data(dto)
                        .id(new Date().toString())
                        .build());*/
    }

    @PostMapping(value = "/rest/api/book/{id}/comment")
    public Mono<CommentDto> save(@PathVariable("id") String bookId, @RequestParam("rating") int rating, @RequestParam("text") String text) {
        return bookRepository.findById(bookId).flatMap(book -> {
            Comment comment = new Comment();
            comment.setText(text);
            comment.setRating(rating);
            comment.setCreated(new Date());
            comment.setBook(book);
            return commentRepository.save(comment);
        }).map(CommentDto::fromEntity);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity handleNotFoundException(EntityNotFoundException e) {
        return new ResponseEntity(e.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity handleException(Exception e) {
        return new ResponseEntity("Во время обработки запроса произошла ошибка: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
