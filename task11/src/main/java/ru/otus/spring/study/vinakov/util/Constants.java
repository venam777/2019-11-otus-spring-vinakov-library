package ru.otus.spring.study.vinakov.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Constants {

    public static final String DATE_FORMAT_PATTERN = "dd.MM.yyyy";
    public static final DateFormat DATE_FORMAT = new SimpleDateFormat(DATE_FORMAT_PATTERN);

}
