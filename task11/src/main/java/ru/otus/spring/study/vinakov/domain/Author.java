package ru.otus.spring.study.vinakov.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;

@Document(collection = "author")
public class Author {

    @Id
    private String id;

    @Field("name")
    private String name;

    @Field("birthday")
    private Date birthday;

    public Author() {}

    public Author(String id) {
        this.id = id;
    }

    public Author(String name, Date birthday) {
        this.name = name;
        this.birthday = birthday;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

}
