package ru.otus.spring.study.vinakov.dto;

import ru.otus.spring.study.vinakov.domain.Book;
import ru.otus.spring.study.vinakov.domain.Comment;

import java.util.LinkedList;
import java.util.List;

public class BookDto {

    private String id;
    private String name;
    private String annotation;
    private AuthorDto author;
    private GenreDto genre;
    private double rating;
    private List<CommentDto> comments = new LinkedList<>();

    public BookDto() {}

    public BookDto(String id, String name, String annotation, AuthorDto author, GenreDto genre) {
        this.id = id;
        this.name = name;
        this.annotation = annotation;
        this.author = author;
        this.genre = genre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    public AuthorDto getAuthor() {
        return author;
    }

    public void setAuthor(AuthorDto author) {
        this.author = author;
    }

    public GenreDto getGenre() {
        return genre;
    }

    public void setGenre(GenreDto genre) {
        this.genre = genre;
    }

    public List<CommentDto> getComments() {
        return comments;
    }

    public void addComment(CommentDto comment) {
        comments.add(comment);
    }

    public void addComment(Comment comment) {
        comments.add(CommentDto.fromEntity(comment));
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public static BookDto fromEntity(Book book) {
        return new BookDto(book.getId(), book.getName(), book.getAnnotation(), AuthorDto.fromEntity(book.getAuthor()), GenreDto.fromEntity(book.getGenre()));
    }
}
