package ru.otus.spring.study.vinakov.controller;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;
import ru.otus.spring.study.vinakov.domain.Author;
import ru.otus.spring.study.vinakov.dto.AuthorDto;
import ru.otus.spring.study.vinakov.repository.AuthorRepository;
import ru.otus.spring.study.vinakov.web.rest.AuthorRestController;

@DataMongoTest
@WebMvcTest
@WebFluxTest(AuthorRestController.class)
@AutoConfigureWebTestClient
//WebTestClient не подтягивается из контекста, null
public class AuthorControllerTest {

    @MockBean
    private AuthorRepository repository;

    @Autowired
    private WebTestClient client;

    @MockBean
    private AuthorRestController authorController;

    @Test
    public void test() {
        /*client.get().uri("/rest/api/author")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(AuthorDto.class);*/
    }
}
