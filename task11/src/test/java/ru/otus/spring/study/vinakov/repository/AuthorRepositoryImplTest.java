package ru.otus.spring.study.vinakov.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;
import ru.otus.spring.study.vinakov.domain.Author;

import java.util.Arrays;
import java.util.Date;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataMongoTest
class AuthorRepositoryImplTest {

    @Autowired
    private AuthorRepository repository;

    private static final String AUTHOR_NAME = "Test author";

    @Test
    void findByName() {
        Mono<Author> mono = repository.save(new Author(AUTHOR_NAME, new Date()));
        StepVerifier.create(mono)
                .as("saving")
                //почему без этого не работает? выдает ошибку java.lang.AssertionError: expectation "expectComplete" failed (expected: onComplete(); actual: onNext(ru.otus.spring.study.vinakov.domain.Author@f2e9d49))
                //а если мне не нужно ничего проверять, а нужно только выполнить сохранение? как это можно сделать?
                .assertNext(author -> assertNotNull(author.getId()))
                .expectComplete()
                .verify();
        Flux<Author> a = repository.findByName(AUTHOR_NAME);
        StepVerifier.create(a)
                .recordWith(Arrays::asList)
                .expectNextCount(1)
                .consumeRecordedWith(authors -> {
                    assertTrue(authors.stream().allMatch(x -> x.getName().equals(AUTHOR_NAME)));
                })
                .verifyComplete();
    }

    @Test
    void testDeleteAll() {
        StepVerifier.create(repository.deleteAll())
                .verifyComplete();
        StepVerifier.create(repository.findAll())
                .expectNextCount(0)
                .verifyComplete();
    }

}