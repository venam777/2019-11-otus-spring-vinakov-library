INSERT INTO AUTHOR(NAME, BIRTHDAY) VALUES ('Пушкин Александр Сергеевич', PARSEDATETIME('06-06-1799','dd-MM-yyyy'));
INSERT INTO AUTHOR(NAME, BIRTHDAY) VALUES ('Лермонтов Михаил Юрьевич', PARSEDATETIME('15-10-1814','dd-MM-yyyy'));
INSERT INTO AUTHOR(NAME, BIRTHDAY) VALUES ('Джон Рональд Руэл Толкин', PARSEDATETIME('03-01-1892','dd-MM-yyyy'));

INSERT INTO GENRE(NAME) VALUES ('Фантастика');
INSERT INTO GENRE(NAME) VALUES ('Роман');
INSERT INTO GENRE(NAME) VALUES ('Фентези');
INSERT INTO GENRE(NAME) VALUES ('Исторический роман');
INSERT INTO GENRE(NAME) VALUES ('Документальный');
INSERT INTO GENRE(NAME) VALUES ('Поэзия');
INSERT INTO GENRE(NAME) VALUES ('Проза');

INSERT INTO BOOK(ID, NAME, ANNOTATION, GENRE_ID, AUTHOR_ID) VALUES (1, 'Евгений Онегин', '«Евгений Онегин» — роман в стихах русского поэта Александра Сергеевича Пушкина, написанный в 1823—1830 годах, одно из самых значительных произведений русской словесности.',
                                                                (SELECT ID FROM GENRE WHERE NAME = 'Роман'), (SELECT ID FROM AUTHOR WHERE NAME = 'Пушкин Александр Сергеевич'));

INSERT INTO BOOK(ID, NAME, ANNOTATION, GENRE_ID, AUTHOR_ID) VALUES (2, 'Мцыри', '«Мцыри» — романтическая поэма М. Ю. Лермонтова, написанная в 1839 году',
                                                                (SELECT ID FROM GENRE WHERE NAME = 'Поэзия'), (SELECT ID FROM AUTHOR WHERE NAME = 'Лермонтов Михаил Юрьевич'));

INSERT INTO USER(USERNAME, NAME, PASSWORD) VALUES ('admin', 'Админ всея библиотеки', '$2y$10$UAFTi.Xd8O68.SzU2qvXc.QgmCQis0e9JxdmI8muRkRBSahcob7Wm');
INSERT INTO USER(USERNAME, NAME, PASSWORD) VALUES ('i.ivanov', 'Иванов Иван Иванович', '$2y$10$d2ntDZKseMbPwPtbJ65tje3EFqvjgOYx/N/uDDNHK7vuuAcO0POyW');