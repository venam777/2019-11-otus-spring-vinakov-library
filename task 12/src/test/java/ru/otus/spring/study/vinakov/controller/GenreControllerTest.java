package ru.otus.spring.study.vinakov.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.otus.spring.study.vinakov.repository.AuthorRepository;
import ru.otus.spring.study.vinakov.repository.UserRepository;
import ru.otus.spring.study.vinakov.web.AuthorController;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Тут у меня небольшой ступор по поводу того, какими аннотациями размечать такие тесты, и есть вопрос
 * Работающие версии:
 * 1. @SpringBootTest + @AutoConfigureMockMvc. Насколько я понимаю, они поднимают весь контекст (репозитории, сервисы, контроллеры + остальные бины)
 * 2. @WebMvcTest(AuthorController.class) + @MockBean на все зависимости (в данном случае, репозитории). Но что если таких
 * зависимостей будет штук 20? замучаешься писать MockBean'ы (
 * Если попытаться разметить аннотациями @WebMvcTest + @DataJpaTest (что вроде как логично, так как нам нужен слой
 * контроллеров и JPA), то получаем ошибку дублирования аннотации @BootstrapWith, так как она есть в каждой из этих аннотаций
 * В связи с этим вопрос: можно ли это сделать как-то красивее, нежели поднимать весь контекст или мокать все репозитории?
 */
//@SpringBootTest
//@AutoConfigureMockMvc
@WebMvcTest(AuthorController.class)
public class GenreControllerTest {

    @MockBean
    private AuthorRepository authorRepository;

    @MockBean
    private UserRepository userRepository;

    @Autowired
    private MockMvc controller;

    @Test
    @WithMockUser(username = "admin", authorities = {"ROLE_ADMIN"})
    public void testGenreAccessDenied() throws Exception {
        assertEquals(controller.perform(MockMvcRequestBuilders.get("/genre/list")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden())
                .andReturn().getResponse().getStatus(), HttpStatus.FORBIDDEN.value());
    }

    /*@Test
    @WithMockUser(username = "i.ivanov", authorities = {"ROLE_USER"})
    public void testGenreOk() throws Exception {
        String content = controller.perform(MockMvcRequestBuilders.get("/genre/list")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertTrue(content.contains("Фантастика"));
        assertTrue(content.contains("Роман"));
    }*/

}
