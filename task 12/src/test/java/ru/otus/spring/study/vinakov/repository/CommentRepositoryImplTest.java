package ru.otus.spring.study.vinakov.repository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import ru.otus.spring.study.vinakov.domain.Book;
import ru.otus.spring.study.vinakov.domain.Comment;
import ru.otus.spring.study.vinakov.dto.BookRating;

import java.util.Date;
import java.util.List;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;
import static org.assertj.core.api.Assertions.*;

@DataJpaTest
@DisplayName("Repository по работе с комментариями ")
public class CommentRepositoryImplTest {

    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private TestEntityManager entityManager;

    private static final Long FIRST_BOOK_ID = 1L;
    private static final Long SECOND_BOOK_ID = 2L;
    private static final Integer FROM_INDEX = 1;
    private static final Integer TO_INDEX = 6;
    private static final Integer COMMENT_COUNT = 5;

    @Test
    @DisplayName(" создает комментарии к книгам")
    void testCreate() {
        commentRepository.save(createComment(FIRST_BOOK_ID, "Comment 1", 4));
        commentRepository.save(createComment(SECOND_BOOK_ID, "Comment 2", 3));
        assertEquals(commentRepository.count(), 2);
    }

    @Test
    @DisplayName(" умеет искать комментарии")
    void testFind(){
        Comment comment = createComment(FIRST_BOOK_ID, "Comment 1", 2);
        entityManager.persist(comment);
        assertEquals(comment.getText(), "Comment 1");
        assertEquals(comment.getBook().getId(), FIRST_BOOK_ID);
        assertEquals(comment.getRating(), 2);
    }

    @Test
    @DisplayName(" умеет обновлять комментарии")
    void testUpdate() {
        Comment comment = createComment(FIRST_BOOK_ID, "Comment 1", 4);
        entityManager.persist(comment);
        comment.setRating(2);
        comment.setText("777");
        commentRepository.save(comment);
        Comment c2 = entityManager.find(Comment.class, comment.getId());
        assertThat(comment).isEqualToComparingFieldByField(c2);
    }

    @Test
    @DisplayName(" умеет удалять комментарии")
    void testDelete() {
        Comment comment = createComment(FIRST_BOOK_ID, "Comment 1", 4);
        entityManager.persist(comment);
        entityManager.flush();
        long id = comment.getId();
        commentRepository.deleteById(comment.getId());
        assertNull(entityManager.find(Comment.class, id));
    }

    @Test
    @DisplayName(" умеет находить комментарии к определенной книге")
    void testFindByBook() {
        IntStream.range(FROM_INDEX, TO_INDEX).forEach(i -> {
            entityManager.persist(createComment(FIRST_BOOK_ID, "Comment " + i, i));
        });
        List<Comment> comments = commentRepository.findByBookId(FIRST_BOOK_ID);
        assertEquals(comments.size(), COMMENT_COUNT);
        comments.forEach(c -> assertEquals(c.getBook().getId(), FIRST_BOOK_ID));
    }

    @Test
    @DisplayName(" умеет получать рейтинг книги на основе комментариев")
    void testGetBookRating() {
        IntStream.range(FROM_INDEX, TO_INDEX).forEach(i -> {
            entityManager.persist(createComment(FIRST_BOOK_ID, "Comment " + i, i));
        });
        assertEquals(commentRepository.getBookRating(FIRST_BOOK_ID).getValue(), 3.0);
        assertEquals(commentRepository.getBookRating(100L).getValue(), 0);
    }

    @Test
    @DisplayName(" умеет находить комментарии по определенной книге с определенной оцекой")
    void testFindByBookAndRating() {
        IntStream.range(FROM_INDEX, TO_INDEX).forEach(i -> {
            entityManager.persist(createComment(FIRST_BOOK_ID, "Comment " + i, i));
        });
        IntStream.range(FROM_INDEX, TO_INDEX).forEach(i -> {
            List<Comment> comments = commentRepository.findByBookIdAndRating(FIRST_BOOK_ID, i);
            assertEquals(comments.size(), 1);
            assertEquals(comments.get(0).getRating(), i);
        });
    }

    @Test
    @DisplayName(" умеет находить последние n комментариев по какой-либо книге")
    void testFindLastCommentsByBook() {
        IntStream.range(FROM_INDEX, TO_INDEX).forEach(i -> {
            entityManager.persist(createComment(FIRST_BOOK_ID, "Comment " + i, i));
        });
        IntStream.range(TO_INDEX, FROM_INDEX).forEach(i -> {
            List<Comment> comments = commentRepository.findLastCommentsByBook(FIRST_BOOK_ID, i);
            assertEquals(comments.size(), i);
            comments.forEach(c -> assertTrue(c.getId() <= i));
        });
    }

    @Test
    @DisplayName(" умеет находить ТОПовые книги, основываясь на оценке в комментариях")
    void testFindTopRatingBooks() {
        IntStream.range(FROM_INDEX, TO_INDEX).forEach(i -> {
            entityManager.persist(createComment(FIRST_BOOK_ID, "Comment " + i, 5));
        });
        IntStream.range(FROM_INDEX, TO_INDEX).forEach(i -> {
            entityManager.persist(createComment(SECOND_BOOK_ID, "Comment " + i, 4));
        });
        List<BookRating> topBooks = commentRepository.findTopRatingBooks(1);
        assertEquals(topBooks.size(), 1);
        assertEquals(topBooks.get(0).getBook().getId(), FIRST_BOOK_ID);
        assertEquals(topBooks.get(0).getRating(), 5);
    }

    @Test
    @DisplayName(" умеет удалять все комментарии к книге")
    void testDeleteAllByBook() {
        IntStream.range(FROM_INDEX, TO_INDEX).forEach(i -> {
            entityManager.persist(createComment(FIRST_BOOK_ID, "Comment " + i, 5));
        });
        IntStream.range(FROM_INDEX, TO_INDEX).forEach(i -> {
            entityManager.persist(createComment(SECOND_BOOK_ID, "Comment " + i, 5));
        });
        commentRepository.deleteAllByBookId(FIRST_BOOK_ID);
        assertThat(commentRepository.findByBookId(FIRST_BOOK_ID)).isEmpty();
        assertEquals(commentRepository.findByBookId(SECOND_BOOK_ID).size(), COMMENT_COUNT);
    }

    private Comment createComment(long bookId, String text, int rating) {
        Comment comment = new Comment();
        comment.setBook(entityManager.find(Book.class, bookId));
        comment.setText(text);
        comment.setRating(rating);
        comment.setCreated(new Date());
        return comment;
    }

}
