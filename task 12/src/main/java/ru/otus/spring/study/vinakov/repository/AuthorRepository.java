package ru.otus.spring.study.vinakov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.otus.spring.study.vinakov.domain.Author;

import java.util.List;

public interface AuthorRepository extends JpaRepository<Author, Long> {

    List<Author> findByName(String name);

}
