package ru.otus.spring.study.vinakov.service;

import org.springframework.transaction.annotation.Transactional;

public interface BookService {

    @Transactional
    void delete(long bookId);
}
