package ru.otus.spring.study.vinakov.dto;

import ru.otus.spring.study.vinakov.domain.Genre;

public class GenreDto {

    private long id;
    private String name;

    public GenreDto() {
    }

    public GenreDto(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static GenreDto fromEntity(Genre genre) {
        return new GenreDto(genre.getId(), genre.getName());
    }
}
