package ru.otus.spring.study.vinakov.service;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import ru.otus.spring.study.vinakov.repository.UserRepository;

import java.util.Collections;
import java.util.Optional;

public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<ru.otus.spring.study.vinakov.domain.User> userOptional = Optional.ofNullable(userRepository.findByUsername(username));
        if (userOptional.isPresent()) {
            ru.otus.spring.study.vinakov.domain.User u = userOptional.get();
            return new User(u.getUsername(), u.getPassword(), Collections.emptyList());
        } else {
            throw new UsernameNotFoundException(String.format("User with username %s not found", username));
        }
    }
}
