package ru.otus.spring.study.vinakov;

import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(HttpMethod.GET, "/");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.formLogin()
                .and()
                .authorizeRequests()
                .antMatchers("/book/**", "/comment/**", "/author/**").authenticated()
                //страницу жанров специально скрыл, чтобы протестить
                .antMatchers("/genre/**").denyAll()
                //только я не понимаю, почему /access-denied не работает без контроллера, просто статическую страницу не показывает (
                .and().exceptionHandling().accessDeniedPage("/access-denied");
    }

    //Перенес юзеров в БД, тут оставил как шпаргалку
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication().withUser("admin").password("$2y$10$UAFTi.Xd8O68.SzU2qvXc.QgmCQis0e9JxdmI8muRkRBSahcob7Wm").roles("ADMIN")
//                .and().withUser("i.ivanov").password("111").roles("USER");
//    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        //return NoOpPasswordEncoder.getInstance();
        return new BCryptPasswordEncoder(10);
    }
}
