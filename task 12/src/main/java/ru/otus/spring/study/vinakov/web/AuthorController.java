package ru.otus.spring.study.vinakov.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.otus.spring.study.vinakov.dto.AuthorDto;
import ru.otus.spring.study.vinakov.repository.AuthorRepository;

import java.util.stream.Collectors;

@Controller
public class AuthorController {

    private final AuthorRepository authorRepository;

    public AuthorController(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @GetMapping("/author/list")
    public String viewAll(Model model) {
        model.addAttribute("authors", authorRepository.findAll().stream().map(AuthorDto::fromEntity).collect(Collectors.toList()));
        return "author/view-all";
    }
}
