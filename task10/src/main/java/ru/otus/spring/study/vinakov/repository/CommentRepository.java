package ru.otus.spring.study.vinakov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.otus.spring.study.vinakov.domain.Comment;
import ru.otus.spring.study.vinakov.util.NotNull;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Long>, CommentRepositoryCustom {

    List<Comment> findByBookId(long bookId);

    @Query("select new ru.otus.spring.study.vinakov.util.NotNull(avg(c.rating), 0.0) from Comment c where c.book.id = :bookId")
    NotNull<Double> getBookRating(@Param("bookId") long bookId);

    List<Comment> findByBookIdAndRating(long bookId, int rating);

    void deleteAllByBookId(long bookId);

}
