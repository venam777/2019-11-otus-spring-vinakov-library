package ru.otus.spring.study.vinakov.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.otus.spring.study.vinakov.domain.Book;

import java.util.List;

public interface BookRepository extends JpaRepository<Book, Long> {

    @EntityGraph("Book.all")
    List<Book> findByName(String name);

    @Override
    @EntityGraph("Book.all")
    List<Book> findAll();
}
