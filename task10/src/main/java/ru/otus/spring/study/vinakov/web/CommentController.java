package ru.otus.spring.study.vinakov.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import ru.otus.spring.study.vinakov.domain.Book;
import ru.otus.spring.study.vinakov.domain.Comment;
import ru.otus.spring.study.vinakov.exception.EntityNotFoundException;
import ru.otus.spring.study.vinakov.repository.BookRepository;
import ru.otus.spring.study.vinakov.repository.CommentRepository;

import java.util.Date;

@Controller
public class CommentController {

    private final CommentRepository commentRepository;
    private final BookRepository bookRepository;

    public CommentController(CommentRepository commentRepository, BookRepository bookRepository) {
        this.commentRepository = commentRepository;
        this.bookRepository = bookRepository;
    }

    @GetMapping("/comment/create")
    public String create(@RequestParam("book") Long bookId, Model model) throws EntityNotFoundException {
        Book book = bookRepository.findById(bookId).orElseThrow(EntityNotFoundException::new);
        Comment comment = new Comment();
        comment.setBook(book);
        model.addAttribute("comment", comment);
        return "comment/edit";
    }

    @PostMapping("/comment/save")
    public RedirectView save(Comment comment) {
        comment.setCreated(new Date());
        commentRepository.save(comment);
        return new RedirectView("/book/" + comment.getBook().getId());
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ModelAndView notFoundHandler(EntityNotFoundException e) {
        return new ModelAndView("not-found").addObject("message", "Книга не найдена");
    }
}
