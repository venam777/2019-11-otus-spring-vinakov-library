package ru.otus.spring.study.vinakov.service;

import org.springframework.stereotype.Component;
import ru.otus.spring.study.vinakov.repository.BookRepository;
import ru.otus.spring.study.vinakov.repository.CommentRepository;

@Component
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;
    private final CommentRepository commentRepository;

    public BookServiceImpl(BookRepository bookRepository, CommentRepository commentRepository) {
        this.bookRepository = bookRepository;
        this.commentRepository = commentRepository;
    }

    @Override
    public void delete(long bookId) {
        commentRepository.deleteAllByBookId(bookId);
        bookRepository.deleteById(bookId);
    }
}
