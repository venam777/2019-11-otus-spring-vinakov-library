package ru.otus.spring.study.vinakov.web.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.otus.spring.study.vinakov.dto.AuthorDto;
import ru.otus.spring.study.vinakov.repository.AuthorRepository;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class AuthorRestController {

    private final AuthorRepository authorRepository;

    public AuthorRestController(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @GetMapping("/rest/api/author")
    public List<AuthorDto> getAll() {
        return authorRepository.findAll().stream().map(AuthorDto::fromEntity).collect(Collectors.toList());
    }


}
