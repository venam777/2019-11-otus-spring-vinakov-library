package ru.otus.spring.study.vinakov.web.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.otus.spring.study.vinakov.domain.Book;
import ru.otus.spring.study.vinakov.dto.BookDto;
import ru.otus.spring.study.vinakov.dto.CommentDto;
import ru.otus.spring.study.vinakov.repository.BookRepository;
import ru.otus.spring.study.vinakov.service.BookCommentService;
import ru.otus.spring.study.vinakov.service.BookService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@SuppressWarnings("unchecked")
public class BookRestController {

    private final BookRepository bookRepository;
    private final BookService bookService;
    private final BookCommentService bookCommentService;

    public BookRestController(BookRepository bookRepository, BookService bookService, BookCommentService bookCommentService) {
        this.bookRepository = bookRepository;
        this.bookService = bookService;
        this.bookCommentService = bookCommentService;
    }

    @GetMapping("/rest/api/book/list")
    public List<BookDto> getAll() {
        return bookRepository.findAll().stream().map(BookDto::fromEntity).collect(Collectors.toList());
    }

    @GetMapping("/rest/api/book/{id}")
    public ResponseEntity<BookDto> getBook(@PathVariable("id") long bookId) {
        Optional<Book> oBook = bookRepository.findById(bookId);
        if (oBook.isPresent()) {
            BookDto dto = BookDto.fromEntity(oBook.get());
            double rating = bookCommentService.getBookRating(bookId);
            dto.setRating((double) ((int) (rating * 10)) / 10d);
            return new ResponseEntity<>(dto, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/rest/api/book/{id}")
    public ResponseEntity delete(@PathVariable("id") long bookId) {
        try {
            bookService.delete(bookId);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity("Error: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/rest/api/book/{id}/comments")
    public List<CommentDto> getComments(@PathVariable("id") long bookId) {
        return bookCommentService.findBookComments(bookId).stream().map(CommentDto::fromEntity).collect(Collectors.toList());
    }

    @PostMapping("/rest/api/book/{id}/comment")
    public ResponseEntity save(@PathVariable("id") long bookId, @RequestParam("rating") int rating, @RequestParam("text") String text) {
        Optional<Book> oBook = bookRepository.findById(bookId);
        if (oBook.isPresent()) {
            bookCommentService.addCommentToBook(bookId, text, rating);
            return ResponseEntity.ok().build();
        } else {
            return new ResponseEntity("Book not found", HttpStatus.NOT_FOUND);
        }

    }

}
