package ru.otus.spring.study.vinakov.dto;

import ru.otus.spring.study.vinakov.domain.Author;
import ru.otus.spring.study.vinakov.util.Constants;

import java.util.Date;

public class AuthorDto {

    private long id;
    private String name;
    private Date birthday;
    private String birthdayString;

    public AuthorDto() {}

    public AuthorDto(long id, String name, Date birthday, String birthdayString) {
        this.id = id;
        this.name = name;
        this.birthday = birthday;
        this.birthdayString = birthdayString;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getBirthdayString() {
        return birthdayString;
    }

    public void setBirthdayString(String birthdayString) {
        this.birthdayString = birthdayString;
    }

    public static AuthorDto fromEntity(Author author) {
        return new AuthorDto(author.getId(), author.getName(), author.getBirthday(), Constants.DATE_FORMAT.format(author.getBirthday()));
    }
}
