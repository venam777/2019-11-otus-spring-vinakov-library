package ru.otus.spring.study.vinakov.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import ru.otus.spring.study.vinakov.domain.Author;
import ru.otus.spring.study.vinakov.domain.Book;
import ru.otus.spring.study.vinakov.domain.Genre;
import ru.otus.spring.study.vinakov.dto.BookDto;
import ru.otus.spring.study.vinakov.dto.CommentDto;
import ru.otus.spring.study.vinakov.exception.EntityNotFoundException;
import ru.otus.spring.study.vinakov.repository.AuthorRepository;
import ru.otus.spring.study.vinakov.repository.BookRepository;
import ru.otus.spring.study.vinakov.repository.GenreRepository;
import ru.otus.spring.study.vinakov.service.BookCommentService;
import ru.otus.spring.study.vinakov.service.BookService;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class BookController {

    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;
    private final GenreRepository genreRepository;
    private final BookService bookService;
    private final BookCommentService bookCommentService;

    public BookController(BookRepository bookRepository, AuthorRepository authorRepository, GenreRepository genreRepository, BookService bookService, BookCommentService bookCommentService) {
        this.bookRepository = bookRepository;
        this.authorRepository = authorRepository;
        this.genreRepository = genreRepository;
        this.bookService = bookService;
        this.bookCommentService = bookCommentService;
    }

    @GetMapping("/book/list")
    public String viewAll(Model model) {
        model.addAttribute("books", bookRepository.findAll());
        return "book/view-all";
    }

    @GetMapping("/book/edit/{id}")
    public String viewBook(@PathVariable("id") Long id, Model model) throws EntityNotFoundException {
        Book book = bookRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        model.addAttribute("authors", getSorted(authorRepository.findAll(), Comparator.comparing(Author::getName)));
        model.addAttribute("genres", getSorted(genreRepository.findAll(), Comparator.comparing(Genre::getName)));
        model.addAttribute("book", book);
        return "book/edit";
    }

    @GetMapping("/book/create")
    public String createBook(Model model) {
        model.addAttribute("book", new Book());
        model.addAttribute("authors", getSorted(authorRepository.findAll(), Comparator.comparing(Author::getName)));
        model.addAttribute("genres", getSorted(genreRepository.findAll(), Comparator.comparing(Genre::getName)));
        return "book/edit";
    }

    @GetMapping("/book/details/{id}")
    public ModelAndView view(@PathVariable("id") Long id) throws EntityNotFoundException {
        Book book = bookRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        ModelAndView modelAndView = new ModelAndView("book/view");
        BookDto bookDto = BookDto.fromEntity(book);
        List<CommentDto> comments = bookCommentService.findBookComments(id).stream().map(CommentDto::fromEntity).collect(Collectors.toList());
        bookDto.setRating(comments.stream().mapToDouble(CommentDto::getRating).average().orElse(0));
        modelAndView.addObject("book", bookDto);
        modelAndView.addObject("comments", comments);
        modelAndView.addObject("rating", String.format("%.1f", bookDto.getRating()));
        //знаю, что так форматировать рейтинг неправильно, нужно в html с помощью thymeleaf, но как я только не пытался завести функцию numbers - ничего не получилось
//            modelAndView.addObject("numbers", new Numbers(LocaleContextHolder.getLocale()));
        return modelAndView;

    }

    @PostMapping("/book/save")
    public RedirectView save(Book book) {
        bookRepository.save(book);
        return new RedirectView("/book/details/" + book.getId());
    }

    //на delete не получилось переделать, спринг выдает: Resolved [org.springframework.web.HttpRequestMethodNotSupportedException: Request method 'DELETE' not supported]
    @GetMapping("/delete/{id}")
    public RedirectView deleteBook(@PathVariable("id") Long id) {
        bookService.delete(id);
        return new RedirectView("/book/list");
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ModelAndView notFoundHandler(EntityNotFoundException e) {
        return new ModelAndView("not-found").addObject("message", "Книга не найдена");
    }

    private <T> List<T> getSorted(List<T> entities, Comparator<T> comparator) {
        entities.sort(comparator);
        return entities;
    }

}
