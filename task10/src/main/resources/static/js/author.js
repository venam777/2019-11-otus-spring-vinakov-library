function listAuthors(containerId) {
    $.ajax({
        url: "/rest/api/author",
        type: 'GET',
        success: function (authors) {
            let container = $("#" + containerId);
            $(container).empty();
            var tbody = "";
            authors.forEach(function (author) {
                tbody += ` <tr>
                                    <td>${author.name}</a>
                                    <td>${author.birthdayString}</td>
                                </tr>`;
            });
            $(container).append(`
                     <table class="pure-table pure-table-horizontal">
                        <thead>
                            <tr>
                                <th>Имя</th>
                                <th>Дата рождения</th>
                            </tr>
                        </thead>
                        <tbody>
                            ${tbody}                        
                        </tbody>
                </table>`);
        }
    });
}