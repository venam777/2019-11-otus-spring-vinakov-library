package ru.otus.spring.study.vinakov.event;

import org.bson.Document;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.AfterDeleteEvent;
import org.springframework.data.mongodb.core.mapping.event.AfterSaveEvent;
import org.springframework.data.mongodb.core.mapping.event.BeforeSaveEvent;
import org.springframework.stereotype.Component;
import ru.otus.spring.study.vinakov.domain.Book;
import ru.otus.spring.study.vinakov.repository.AuthorRepository;
import ru.otus.spring.study.vinakov.repository.CommentRepository;
import ru.otus.spring.study.vinakov.repository.GenreRepository;

import java.util.Objects;

@Component
public class MongoBookEventListener extends AbstractMongoEventListener<Book> {

    private final AuthorRepository authorRepository;
    private final GenreRepository genreRepository;
    private final CommentRepository commentRepository;

    public MongoBookEventListener(AuthorRepository authorRepository, GenreRepository genreRepository, CommentRepository commentRepository) {
        this.authorRepository = authorRepository;
        this.genreRepository = genreRepository;
        this.commentRepository = commentRepository;
    }

    @Override
    public void onAfterDelete(AfterDeleteEvent<Book> event) {
        super.onAfterDelete(event);
        Document source = event.getSource();
        String id = source.get("_id").toString();
        commentRepository.deleteAllByBookId(id);
    }

    @Override
    public void onBeforeSave(BeforeSaveEvent<Book> event) {
        super.onBeforeSave(event);
        Book book = event.getSource();
        if (Objects.isNull(book.getGenre().getId())) {
            book.setGenre(genreRepository.save(book.getGenre()));
        }
        if (Objects.isNull(book.getAuthor().getId())) {
            book.setAuthor(authorRepository.save(book.getAuthor()));
        }
    }
}
