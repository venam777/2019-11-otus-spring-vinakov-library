package ru.otus.spring.study.vinakov.shell;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import ru.otus.spring.study.vinakov.domain.Author;
import ru.otus.spring.study.vinakov.domain.Book;
import ru.otus.spring.study.vinakov.domain.Genre;
import ru.otus.spring.study.vinakov.exception.EntityNotFoundException;
import ru.otus.spring.study.vinakov.repository.AuthorRepository;
import ru.otus.spring.study.vinakov.repository.BookRepository;
import ru.otus.spring.study.vinakov.repository.GenreRepository;
import ru.otus.spring.study.vinakov.util.EntityFactory;
import ru.otus.spring.study.vinakov.util.EntityPrinter;
import ru.otus.spring.study.vinakov.util.Messages;

import java.util.Optional;
import java.util.stream.Collectors;

@ShellComponent
public class BookShellOperations {

    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;
    private final GenreRepository genreRepository;

    private static final String AUTHOR_ENTITY = "Автор";
    private static final String GENRE_ENTITY = "Жанр";
    private static final String BOOK_ENTITY = "Книга";

    public BookShellOperations(BookRepository bookRepository, AuthorRepository authorRepository, GenreRepository genreRepository) {
        this.bookRepository = bookRepository;
        this.authorRepository = authorRepository;
        this.genreRepository = genreRepository;
    }

    @ShellMethod(value = "Добавление новой книги", key = "create-book")
    public String createBook(String name, @ShellOption(defaultValue = "") String annotation, @ShellOption String authorId, @ShellOption String genreId) {
        try {
            Author author = authorRepository.findById(authorId).orElseThrow(() -> new EntityNotFoundException(Messages.entityNotFoundError(AUTHOR_ENTITY)));
            Genre genre = genreRepository.findById(genreId).orElseThrow(() -> new EntityNotFoundException(Messages.entityNotFoundError(GENRE_ENTITY)));
            Book book = EntityFactory.createEntity(Book.class);
            book.setName(name);
            book.setAnnotation(annotation);
            book.setGenre(genre);
            book.setAuthor(author);
            bookRepository.save(book);
            return Messages.entityCreated("Книга", book.getId().toString());
        } catch (Exception e) {
            return Messages.unexpectedError(e.getMessage());
        }
    }

    @ShellMethod(value = "Обновление информации о книге", key = {"update-book"})
    public String updateBook(String id,
                             @ShellOption(defaultValue = ShellOption.NULL) String name,
                             @ShellOption(defaultValue = ShellOption.NULL) String annotation,
                             @ShellOption(defaultValue = ShellOption.NULL) String authorId,
                             @ShellOption(defaultValue = ShellOption.NULL) String genreId) {
        Book book = bookRepository.findById(id).orElseThrow(() -> new EntityNotFoundException(Messages.entityNotFoundError(BOOK_ENTITY)));
        book.setName(paramExist(name) ? name : book.getName());
        book.setAnnotation(paramExist(annotation) ? annotation : book.getAnnotation());
        book.setAuthor(authorRepository.findById(authorId).orElseThrow(() -> new EntityNotFoundException(Messages.entityNotFoundError(AUTHOR_ENTITY))));
        book.setGenre(genreRepository.findById(genreId).orElseThrow(() -> new EntityNotFoundException(Messages.entityNotFoundError(GENRE_ENTITY))));
        try {
            bookRepository.save(book);
            return Messages.entityUpdated("Книга", id);
        } catch (Exception e) {
            return Messages.unexpectedError(e.getMessage());
        }
    }

    @ShellMethod(value = "Ищет книгу по названию или ID", key = "find-book")
    public String findBook(String id, String name) {
        try {
            Optional<Book> book = bookRepository.findById(id);
            if (book.isPresent()) {
                return EntityPrinter.printFull(book.get());
            } else {
                StringBuilder sb = new StringBuilder();
                Optional.ofNullable(bookRepository.findByName(name)).ifPresent(list -> {
                    list.forEach(b -> sb.append(EntityPrinter.printFull(b)));
                });
                return sb.toString().isEmpty() ? Messages.entityNotFoundError("Книга") : sb.toString();
            }
        } catch (Exception e) {
            return Messages.unexpectedError(e.getMessage());
        }
    }

    @ShellMethod(value = "Просмотр всех книг", key = "list-book")
    public String findAllBooks() {
        return bookRepository.findAll().stream().map(EntityPrinter::printFull).collect(Collectors.joining("\n"));
    }

    @ShellMethod(value = "Удаляет книгу по id", key = "delete-book")
    public String deleteBook(String id) {
        if (paramExist(id)) {
            bookRepository.deleteById(id);
            return Messages.entityDeleted("Книга", id);
        } else {
            return Messages.emptyParamError("id");
        }
    }

    private boolean paramExist(Object param) {
        return param instanceof String ? !ShellOption.NONE.equals(param) && !ShellOption.NULL.equals(param) : param != null;
    }
}
