package ru.otus.spring.study.vinakov.repository;

import org.springframework.data.repository.query.Param;
import ru.otus.spring.study.vinakov.domain.Comment;
import ru.otus.spring.study.vinakov.dto.BookRating;
import ru.otus.spring.study.vinakov.util.NotNull;

import java.util.List;

public interface CommentRepositoryCustom {

    List<Comment> findLastCommentsByBook(String bookId, int count);

    List<BookRating> findTopRatingBooks(int count);

    double getBookRating(@Param("bookId") String bookId);

}
