package ru.otus.spring.study.vinakov.mongo;

import com.github.cloudyrock.mongock.Mongock;
import com.github.cloudyrock.mongock.SpringMongockBuilder;
import com.mongodb.MongoClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MongockConfig {

    @Bean
    public Mongock mongock(MongoClient mongoClient, MongoProps props) {
        return new SpringMongockBuilder(mongoClient, props.getDatabase(), DatabaseChangelog.class.getPackageName())
                .build();
    }
}
