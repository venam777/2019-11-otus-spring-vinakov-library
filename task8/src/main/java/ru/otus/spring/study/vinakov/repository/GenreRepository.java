package ru.otus.spring.study.vinakov.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.otus.spring.study.vinakov.domain.Genre;

import java.util.List;

@SuppressWarnings("unused")
public interface GenreRepository extends MongoRepository<Genre, String> {

    List<Genre> findByName(String name);

}
