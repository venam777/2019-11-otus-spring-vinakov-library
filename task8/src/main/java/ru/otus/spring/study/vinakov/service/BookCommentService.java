package ru.otus.spring.study.vinakov.service;

import ru.otus.spring.study.vinakov.domain.Comment;

import java.util.List;

public interface BookCommentService {

    String addCommentToBook(String bookId, String text, int rating);

    List<Comment> findBookComments(String bookId);

    double getBookRating(String bookId);

    List<Comment> findCommentsByRating(String bookId, int rating);

    void deleteCommentsByBook(String bookId);

}
