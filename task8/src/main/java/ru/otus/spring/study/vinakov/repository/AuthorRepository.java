package ru.otus.spring.study.vinakov.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.otus.spring.study.vinakov.domain.Author;

import java.util.List;

public interface AuthorRepository extends MongoRepository<Author, String> {

    List<Author> findByName(String name);

}
