package ru.otus.spring.study.vinakov.mongo;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import ru.otus.spring.study.vinakov.domain.Author;
import ru.otus.spring.study.vinakov.domain.Book;
import ru.otus.spring.study.vinakov.domain.Genre;
import ru.otus.spring.study.vinakov.util.Constants;

import java.text.ParseException;
import java.util.*;

@ChangeLog(order = "001")
public class DatabaseChangelog {

    @ChangeSet(order = "000", id = "dropDB", author = "a.vinakov")
    public void dropDatabase(MongoDatabase database) {
        database.drop();
    }

    @ChangeSet(order = "001", id = "fillAuthors", author = "a.vinakov")
    public void fillAuthorTable(MongoDatabase db) throws ParseException {
        MongoCollection<Document> authors = db.getCollection(getCollectionName(Author.class));
        List<Document> entities = new LinkedList<>();
        entities.add(newAuthor("Пушкин Александр Сергеевич", "06.06.1799"));
        entities.add(newAuthor("Лермонтов Михаил Юрьевич", "15.10.1814"));
        entities.add(newAuthor("Джон Рональд Руэл Толкин", "03.01.1892"));
        authors.insertMany(entities);
//        template.save(new Author("Пушкин Александр Сергеевич", Constants.DATE_FORMAT.parse("06.06.1799")));
//        template.save(new Author("Лермонтов Михаил Юрьевич", Constants.DATE_FORMAT.parse("15.10.1814")));
//        template.save(new Author("Джон Рональд Руэл Толкин", Constants.DATE_FORMAT.parse("03.01.1892")));
    }

    @ChangeSet(order = "002", id = "fillGenres", author = "a.vinakov")
    public void fillGenreTable(MongoDatabase db) {
        MongoCollection<Document> genres = db.getCollection(getCollectionName(Genre.class));
        List<Document> entities = new LinkedList<>();
        entities.add(new Document("name", "Фантастика"));
        entities.add(new Document("name", "Роман"));
        entities.add(new Document("name", "Фентези"));
        entities.add(new Document("name", "Исторический роман"));
        entities.add(new Document("name", "Документальный"));
        entities.add(new Document("name", "Поэзия"));
        entities.add(new Document("name", "Проза"));
        genres.insertMany(entities);
    }

    @ChangeSet(order = "003", id = "fillBooks", author = "a.vinakov")
    public void fillBookTable(MongoTemplate mongoTemplate) {
        mongoTemplate.save(new Book("Евгений Онегин", "«Евгений Онегин» — роман в стихах русского поэта Александра Сергеевича Пушкина, написанный в 1823—1830 годах",
                findByName(mongoTemplate, Genre.class, "Роман"), findByName(mongoTemplate, Author.class, "Пушкин Александр Сергеевич")));
        mongoTemplate.save(new Book("Мцыри", "Мцыри» — романтическая поэма М. Ю. Лермонтова, написанная в 1839 году",
                findByName(mongoTemplate, Genre.class, "Роман"), findByName(mongoTemplate, Author.class, "Лермонтов Михаил Юрьевич")));
    }

    /*@ChangeSet(order = "004", id = "fillComments", author = "a.vinakov")
    public void fillCommentTable(MongoTemplate mongoTemplate) {
        mongoTemplate.save(new Comment("Отлично!", new Date(), 5, findByName(mongoTemplate, Book.class, "Евгений Онегин")));
        mongoTemplate.save(new Comment("Книга понравилась", new Date(), 4, findByName(mongoTemplate, Book.class, "Евгений Онегин")));
        mongoTemplate.save(new Comment("Немного скучная", new Date(), 4, findByName(mongoTemplate, Book.class, "Мцыри")));
    }*/

    private Document newAuthor(String name, String birthday) throws ParseException {
        Map<String, Object> values = new HashMap<>();
        values.put("name", name);
        values.put("birthday", Constants.DATE_FORMAT.parse(birthday));
        return new Document(values);
    }

    private <T> T findByName(MongoTemplate template, Class<T> clazz, String name) {
        return template.find(Query.query(Criteria.where("name").is(name)), clazz).get(0);
    }

    private String getCollectionName(Class<?> entityClass) {
        Class<org.springframework.data.mongodb.core.mapping.Document> annotation = org.springframework.data.mongodb.core.mapping.Document.class;
        if (entityClass.isAnnotationPresent(annotation)) {
            String collectionName = entityClass.getAnnotation(org.springframework.data.mongodb.core.mapping.Document.class).collection();
            if (!collectionName.isEmpty()) {
                return collectionName;
            } else {
                return entityClass.getAnnotation(org.springframework.data.mongodb.core.mapping.Document.class).value();
            }
        } else
            throw new RuntimeException("Entity " + entityClass.getSimpleName() + " is not marked by @Document annotation");
    }

}
