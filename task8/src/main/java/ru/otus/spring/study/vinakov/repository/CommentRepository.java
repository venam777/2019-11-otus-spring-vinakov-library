package ru.otus.spring.study.vinakov.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.otus.spring.study.vinakov.domain.Book;
import ru.otus.spring.study.vinakov.domain.Comment;

import java.util.List;

public interface CommentRepository extends MongoRepository<Comment, String>, CommentRepositoryCustom {

    List<Comment> findByBookId(String bookId);

    List<Comment> findByBookIdAndRating(String bookId, int rating);

    void deleteAllByBookId(String bookId);

}