package ru.otus.spring.study.vinakov.service;

import org.springframework.stereotype.Component;
import ru.otus.spring.study.vinakov.domain.Book;
import ru.otus.spring.study.vinakov.domain.Comment;
import ru.otus.spring.study.vinakov.exception.EntityNotFoundException;
import ru.otus.spring.study.vinakov.repository.BookRepository;
import ru.otus.spring.study.vinakov.repository.CommentRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Component
public class BookCommentServiceImpl implements BookCommentService {

    private final CommentRepository commentRepository;
    private final BookRepository bookRepository;

    public BookCommentServiceImpl(CommentRepository commentRepository, BookRepository bookRepository) {
        this.commentRepository = commentRepository;
        this.bookRepository = bookRepository;
    }

    @Override
    public String addCommentToBook(String bookId, String text, int rating) {
        Optional<Book> book = bookRepository.findById(bookId);
        if (book.isPresent()) {
            Book b = book.get();
            Comment comment = new Comment();
            comment.setText(text);
            comment.setCreated(new Date());
            comment.setRating(rating);
            comment.setBook(b);
            commentRepository.save(comment);
            return comment.getId();
        } else throw new EntityNotFoundException(String.format("Book with id %s not found", bookId));
    }

    @Override
    public List<Comment> findBookComments(String bookId) {
        return commentRepository.findByBookId(bookId);
    }

    @Override
    public double getBookRating(String bookId) {
        return commentRepository.getBookRating(bookId);
    }

    @Override
    public List<Comment> findCommentsByRating(String bookId, int rating) {
        return commentRepository.findByBookIdAndRating(bookId, rating);
    }

    @Override
    public void deleteCommentsByBook(String bookId) {
        commentRepository.deleteAllByBookId(bookId);
    }
}
