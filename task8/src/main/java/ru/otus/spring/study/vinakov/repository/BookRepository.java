package ru.otus.spring.study.vinakov.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.otus.spring.study.vinakov.domain.Book;

import java.util.List;

public interface BookRepository extends MongoRepository<Book, String> {

    List<Book> findByName(String name);

    void deleteAllByAuthorId(String authorId);

    void deleteAllByGenreId(String genreId);
}
