package ru.otus.spring.study.vinakov.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.*;
import ru.otus.spring.study.vinakov.util.Constants;

import java.math.BigInteger;
import java.util.Date;

@Document(collection = "comment")
public class Comment {

    @Id
    private String id;
    @Field
    private String text;
    @Field
    private Date created;
    @Field
    private int rating;
    @DBRef
    private Book book;

    public Comment() {}

    public Comment(String id) {
        this.id = id;
    }

    public Comment(String text, Date created, int rating, Book book) {
        this.text = text;
        this.created = created;
        this.rating = rating;
        this.book = book;
    }

    public String  getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
}
