package ru.otus.spring.study.vinakov.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "book")
public class Book {

    @Id
    private String id;

    @Field("name")
    private String name;

    @Field("annotation")
    private String annotation;

    @DBRef
    private Genre genre;

    @DBRef
    private Author author;

    public Book() {}

    public Book(String id) {
        this.id = id;
    }

    public Book(String name, String annotation, Genre genre, Author author) {
        this.name = name;
        this.annotation = annotation;
        this.genre = genre;
        this.author = author;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }
}
