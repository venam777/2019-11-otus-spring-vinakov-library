package ru.otus.spring.study.vinakov.repository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.context.annotation.ComponentScan;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataMongoTest
@DisplayName("Repository книг должно ")
@ComponentScan({"ru.otus.spring.study.vinakov.mongo"})
class BookRepositoryImplTest {

    @Autowired
    private BookRepository bookRepository;

    private static String FIRST_BOOK_NAME = "Евгений Онегин";
    private static String SECOND_BOOK_NAME = "Мцыри";

    @Test
    @DisplayName(" искать книги по названию")
    void findByName() {
        assertEquals(bookRepository.findByName(FIRST_BOOK_NAME).get(0).getName(), FIRST_BOOK_NAME);
        assertEquals(bookRepository.findByName(SECOND_BOOK_NAME).get(0).getName(), SECOND_BOOK_NAME);
    }


}