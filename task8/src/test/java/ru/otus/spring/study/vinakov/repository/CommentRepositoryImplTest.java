package ru.otus.spring.study.vinakov.repository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import ru.otus.spring.study.vinakov.domain.Book;
import ru.otus.spring.study.vinakov.domain.Comment;
import ru.otus.spring.study.vinakov.dto.BookRating;

import java.util.Date;
import java.util.List;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataMongoTest
@DisplayName("Repository по работе с комментариями ")
@ComponentScan({"ru.otus.spring.study.vinakov.mongo"})
public class CommentRepositoryImplTest {

    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private MongoOperations mongoOperations;

    private static Book FIRST_BOOK;
    private static Book SECOND_BOOK;
    private static final Integer FROM_INDEX = 1;
    private static final Integer TO_INDEX = 6;
    private static final Integer COMMENT_COUNT = 5;

    @BeforeEach
    void findBooks() {
        mongoOperations.dropCollection(Comment.class);
        FIRST_BOOK = mongoOperations.find(Query.query(Criteria.where("name").is("Евгений Онегин")), Book.class).get(0);
        SECOND_BOOK = mongoOperations.find(Query.query(Criteria.where("name").is("Мцыри")), Book.class).get(0);
    }

    @Test
    @DisplayName(" создает комментарии к книгам")
    void testCreate() {
        commentRepository.save(createComment(FIRST_BOOK, "Comment 1", 4));
        commentRepository.save(createComment(SECOND_BOOK, "Comment 2", 3));
        assertEquals(commentRepository.count(), 2);
    }

    @Test
    @DisplayName(" умеет искать комментарии")
    void testFind(){
        Comment comment = createComment(FIRST_BOOK, "Comment 1", 2);
        mongoOperations.save(comment);
        assertEquals(comment.getText(), "Comment 1");
        assertEquals(comment.getBook().getId(), FIRST_BOOK.getId());
        assertEquals(comment.getRating(), 2);
    }

    @Test
    @DisplayName(" умеет обновлять комментарии")
    void testUpdate() {
        Comment comment = createComment(FIRST_BOOK, "Comment 1", 4);
        mongoOperations.save(comment);
        comment.setRating(2);
        comment.setText("777");
        commentRepository.save(comment);
        Comment c2 = mongoOperations.findById(comment.getId(), Comment.class);
        assertThat(comment).isEqualToComparingOnlyGivenFields(c2, "id", "text", "created", "rating");
    }

    @Test
    @DisplayName(" умеет удалять комментарии")
    void testDelete() {
        Comment comment = createComment(FIRST_BOOK, "Comment 1", 4);
        mongoOperations.save(comment);
        String id = comment.getId();
        commentRepository.deleteById(comment.getId());
        assertNull(mongoOperations.findById(id, Comment.class));
    }

    @Test
    @DisplayName(" умеет находить комментарии к определенной книге")
    void testFindByBook() {
        IntStream.range(FROM_INDEX, TO_INDEX).forEach(i -> {
            mongoOperations.save(createComment(FIRST_BOOK, "Comment " + i, i));
        });
        List<Comment> comments = commentRepository.findByBookId(FIRST_BOOK.getId());
        assertEquals(comments.size(), COMMENT_COUNT);
        comments.forEach(c -> assertEquals(c.getBook().getId(), FIRST_BOOK.getId()));
    }

    @Test
    @DisplayName(" умеет получать рейтинг книги на основе комментариев")
    void testGetBookRating() {
        IntStream.range(FROM_INDEX, TO_INDEX).forEach(i -> {
            mongoOperations.save(createComment(FIRST_BOOK, "Comment " + i, i));
        });
        IntStream.range(FROM_INDEX, TO_INDEX).forEach(i -> {
            mongoOperations.save(createComment(SECOND_BOOK, "Comment " + i, 2));
        });
        assertEquals(commentRepository.getBookRating(FIRST_BOOK.getId()), 3.0);
        assertEquals(commentRepository.getBookRating(SECOND_BOOK.getId()), 2.0);
    }

    @Test
    @DisplayName(" умеет находить комментарии по определенной книге с определенной оцекой")
    void testFindByBookAndRating() {
        IntStream.range(FROM_INDEX, TO_INDEX).forEach(i -> {
            mongoOperations.save(createComment(FIRST_BOOK, "Comment " + i, i));
        });
        IntStream.range(FROM_INDEX, TO_INDEX).forEach(i -> {
            List<Comment> comments = commentRepository.findByBookIdAndRating(FIRST_BOOK.getId(), i);
            assertEquals(comments.size(), 1);
            assertEquals(comments.get(0).getRating(), i);
        });
    }

    @Test
    @DisplayName(" умеет находить последние n комментариев по какой-либо книге")
    void testFindLastCommentsByBook() {
        Book book = mongoOperations.save(new Book("Test", "Test", null, null));
        IntStream.range(FROM_INDEX, TO_INDEX).forEach(i -> {
            mongoOperations.save(createComment(book, "Comment " + i, i));
        });
        IntStream.range(TO_INDEX, FROM_INDEX).forEach(i -> {
            List<Comment> comments = commentRepository.findLastCommentsByBook(book.getId(), i);
            assertEquals(comments.size(), i);
        });
    }

    @Test
    @DisplayName(" умеет находить ТОПовые книги, основываясь на оценке в комментариях")
    void testFindTopRatingBooks() {
        IntStream.range(FROM_INDEX, TO_INDEX).forEach(i -> {
            mongoOperations.save(createComment(FIRST_BOOK, "Comment " + i, 5));
        });
        IntStream.range(FROM_INDEX, TO_INDEX).forEach(i -> {
            mongoOperations.save(createComment(SECOND_BOOK, "Comment " + i, 4));
        });
        List<BookRating> topBooks = commentRepository.findTopRatingBooks(1);
        assertEquals(topBooks.size(), 1);
        assertThat(topBooks.get(0).getBook()).isEqualToComparingOnlyGivenFields(FIRST_BOOK, "id", "name", "author.id", "genre.id");
        assertEquals(topBooks.get(0).getRating(), 5);
    }

    @Test
    @DisplayName(" умеет удалять все комментарии к книге")
    void testDeleteAllByBook() {
        IntStream.range(FROM_INDEX, TO_INDEX).forEach(i -> {
            mongoOperations.save(createComment(FIRST_BOOK, "Comment " + i, 5));
        });
        IntStream.range(FROM_INDEX, TO_INDEX).forEach(i -> {
            mongoOperations.save(createComment(SECOND_BOOK, "Comment " + i, 5));
        });
        commentRepository.deleteAllByBookId(FIRST_BOOK.getId());
        assertThat(commentRepository.findByBookId(FIRST_BOOK.getId()).isEmpty());
        assertEquals(commentRepository.findByBookId(SECOND_BOOK.getId()).size(), COMMENT_COUNT);
    }

    private Comment createComment(Book book, String text, int rating) {
        Comment comment = new Comment();
        comment.setBook(book);
        comment.setText(text);
        comment.setRating(rating);
        comment.setCreated(new Date());
        return comment;
    }

}
