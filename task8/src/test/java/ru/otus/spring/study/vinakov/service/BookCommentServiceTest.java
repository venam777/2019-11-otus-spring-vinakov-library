package ru.otus.spring.study.vinakov.service;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import ru.otus.spring.study.vinakov.domain.Book;
import ru.otus.spring.study.vinakov.domain.Comment;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@Import(BookCommentServiceImpl.class)
@DisplayName("Сервис по работе с комментариями ")
@ComponentScan({"ru.otus.spring.study.vinakov.mongo"})
class BookCommentServiceTest {

    @Autowired
    private BookCommentServiceImpl bookCommentService;

    @Autowired
    private MongoOperations mongoOperations;

    @Test
    @DisplayName(" умеет добавлять новые комменты")
    void testCreateComments() {
        Book book = mongoOperations.aggregate(Aggregation.newAggregation(Aggregation.limit(1)), Book.class, Book.class).getUniqueMappedResult();
        bookCommentService.addCommentToBook(book.getId(), "Comment 1", 4);
        bookCommentService.addCommentToBook(book.getId(), "Comment 2", 5);
        bookCommentService.addCommentToBook(book.getId(), "Comment 3", 2);
        List<Comment> comments = bookCommentService.findBookComments(book.getId());
        assertEquals(comments.size(), 3);
        comments.forEach(c -> assertEquals(c.getBook().getId(), book.getId()));
    }

    //остальные методы наверное нет смысла тестировать, т.к. они обертка над DAO. для DAO Тесты есть
}
