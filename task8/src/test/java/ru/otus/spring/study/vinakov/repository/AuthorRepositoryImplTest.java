package ru.otus.spring.study.vinakov.repository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import ru.otus.spring.study.vinakov.domain.Author;
import ru.otus.spring.study.vinakov.domain.Book;
import ru.otus.spring.study.vinakov.domain.Genre;
import ru.otus.spring.study.vinakov.mongo.MongockConfig;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataMongoTest
@DisplayName("Repository для работы с авторами должно ")
@Import(MongockConfig.class)
@ComponentScan({"ru.otus.spring.study.vinakov.mongo"})
class AuthorRepositoryImplTest {

    @Autowired
    private AuthorRepository authorRepository;
    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private GenreRepository genreRepository;

    private static final String AUTHOR_NAME = "Test author";

    @Test
    void findByName() {
        authorRepository.save(createTestAuthor());
        List<Author> authors = authorRepository.findByName(AUTHOR_NAME);
        assertEquals(authors.size(), 1);
        List<Book> books = bookRepository.findAll();
        List<Genre> genres = genreRepository.findAll();
        assertEquals(authors.get(0).getName(), AUTHOR_NAME);
    }

    private Author createTestAuthor() {
        Author author = new Author();
        author.setName(AUTHOR_NAME);
        author.setBirthday(new Date());
        return author;
    }
}